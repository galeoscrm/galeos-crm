<?php
    class AccountSync extends Item
    {
        public function __toString()
        {
            return $this->id;
        }

        public static function getModuleClassName()
        {
            return 'AccountSyncModule';
        }

        /**
         * Returns the display name for the model class.
         * @return dynamic label name based on module.
         */
        protected static function getLabel($language = null)
        {
            return 'AccountSync';
        }

        /**
         * Returns the display name for plural of the model class.
         * @return dynamic label name based on module.
         */
        protected static function getPluralLabel($language = null)
        {
            return 'AccountsSync';
        }

        public static function canSaveMetadata()
        {
            return true;
        }

        public static function getDefaultMetadata()
        {
            $metadata = parent::getDefaultMetadata();
            $metadata[__CLASS__] = array(
                'members' => array(
                    'varioId',
                	'lmsId',
                	'processed',
                	'processedTime',
                ),
                'relations' => array(
                	'account'             => array(static::HAS_ONE,   'Account'),
                ),
                'rules' => array(
                    array('varioId',   		  'type',    'type' => 'string'),
                	array('lmsId',   		  'type',    'type' => 'string'),
                	array('processed',   	  'type',    'type' => 'boolean'),
                	array('processedTime',    'type',    'type' => 'datetime'),
                ),
                'elements' => array(
                	'account'                 => 'Account',
                ),
                'customFields' => array(
                ),
                'defaultSortAttribute' => 'name',
                'noAudit' => array(
                	'varioId',
                	'lmsId',
                	'processed',
                	'processedTime',
                ),
            );
            return $metadata;
        }

        public static function isTypeDeletable()
        {
            return true;
        }
        
        public static function getByAccount(Account $account)
        {
        	assert('$account->id > 0');
        	$searchAttributeData = array();
        	$searchAttributeData['clauses'] = array(
        			1 => array(
        					'attributeName'        => 'account',
        					'operatorType'         => 'equals',
        					'value'                => $account->id
        			),
        	);
        	$searchAttributeData['structure'] = '1';
        	$joinTablesAdapter = new RedBeanModelJoinTablesQueryAdapter('AccountSync');
        	$where             = RedBeanModelDataProvider::makeWhere('AccountSync', $searchAttributeData, $joinTablesAdapter);
        	$result = self::getSubset($joinTablesAdapter, null, null, $where);
        	if ($result == null)
	       		return null;
        	else
        		return $result[0];
        }
    }
?>
