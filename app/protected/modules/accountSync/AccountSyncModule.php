<?php
    class AccountSyncModule extends Module
    {

    	public function getDependencies()
        {
            return array(
                'zurmo',
            );
        }

        public function getRootModelNames()
        {
            return array('AccountSync');
        }

        public static function getDefaultMetadata()
        {
            $metadata = array();
            return $metadata;
        }

        public static function getPrimaryModelName()
        {
            return 'AccountSync';
        }
        
	
		public static function modelsAreNeverGloballySearched()
		{
			return true;
		}
		
    }
?>
