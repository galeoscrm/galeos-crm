<?php
    
    class BindRegisteredDomainElement extends TextElement
    {
        protected function renderLabel()
        {
            $title       = Zurmo::t('ZurmoModule', 'Like: dc=username,dc=server,dc=world for 389 Directory Server and Open LDAP'); 
            $content     = parent::renderLabel();
            $content    .= '<span id="ldap-rollup-tooltip" class="tooltip" title="' . $title . '">?</span>';
            $qtip = new ZurmoTip();
            $qtip->addQTip("#ldap-rollup-tooltip");
            return $content;
        }
    }
?>