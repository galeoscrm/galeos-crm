<?php
    class Link extends Item implements StarredInterface
    {
        public function __toString()
        {
            if (trim($this->name) == '')
            {
                return Yii::t('Default', '(Unnamed)');
            }
            return $this->name;
        }

        public static function getModuleClassName()
        {
            return 'LinksModule';
        }

        /**
         * Returns the display name for the model class.
         * @return dynamic label name based on module.
         */
        protected static function getLabel($language = null)
        {
            return 'Link';
        }

        /**
         * Returns the display name for plural of the model class.
         * @return dynamic label name based on module.
         */
        protected static function getPluralLabel($language = null)
        {
            return 'Links';
        }

        public static function canSaveMetadata()
        {
            return true;
        }

        public static function getDefaultMetadata()
        {
            $metadata = parent::getDefaultMetadata();
            $metadata[__CLASS__] = array(
                'members' => array(
                    'name',
                	'url',
                ),
                'relations' => array(
                	'account'             => array(static::HAS_ONE,   'Account'),
                ),
                'rules' => array(
                    array('name',     		  'required'),
                    array('name',     		  'type',    'type' => 'string'),
                    array('name',     		  'length',  'max' => 200),
                	array('url',              'required'),
                	array('url',              'url',     'defaultScheme' => 'http'),
                		
                ),
                'elements' => array(
                	'account'                 => 'Account',
                ),
                'customFields' => array(
                ),
                'defaultSortAttribute' => 'name',
                'noAudit' => array(
                		'name',
				'url'
                ),
            );
            return $metadata;
        }

        public static function isTypeDeletable()
        {
            return true;
        }
        
    }
?>
