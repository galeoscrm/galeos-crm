<?php
     class LinkurlListViewColumnAdapter extends TextListViewColumnAdapter
    {
        public function renderGridViewData()
        {
        	return array(
                'name'  => $this->attribute,
                'value' => 'ZurmoHtml::link($data->name, $data->url, array("target" => "blank","title" => "Open " . $data->url));',
                'type'  => 'raw',
            );
        }
    }
?>