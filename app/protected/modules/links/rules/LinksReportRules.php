<?php
    /**
     * Report rules to be used with the Accounts module.
     */
    class LinksReportRules extends SecuredReportRules
    {
        /**
         * @return array
         */
        public static function getDefaultMetadata()
        {
            $metadata = array(
                'Link' => array(
                    'nonReportable' =>
                        array('links', 'link', 'url'),
                )
            );
            return array_merge(parent::getDefaultMetadata(), $metadata);
        }
    }
?>