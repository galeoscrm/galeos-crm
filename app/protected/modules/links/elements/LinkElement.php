<?php
    class LinkElement extends ModelElement
    {
        protected static $moduleId = 'links';

        
        protected function renderControlEditable()
        {
            assert('$this->model->{$this->attribute} instanceof Link');
            return parent::renderControlEditable();
        }
    }
?>
