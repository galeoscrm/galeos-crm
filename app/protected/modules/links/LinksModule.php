<?php
    class LinksModule extends SecurableModule
    {
        const RIGHT_CREATE_LINKS = 'Create Links';
        const RIGHT_DELETE_LINKS = 'Delete Links';
       
        public function getDependencies()
        {
            return array(
             	'configuration',
                'zurmo',
            );
        }

        public function getRootModelNames()
        {
            return array('Link');
        }
        
        public static function getTranslatedRightsLabels()
        {
        	$labels                              = array();
        	$labels[self::RIGHT_CREATE_LINKS] = 'Create Links';
        	$labels[self::RIGHT_DELETE_LINKS] = 'Delete Links';
        	return $labels;
        }

        public static function getDefaultMetadata()
        {
            $metadata = array();
            $metadata['global'] = array(
            	'designerMenuItems' => array(
            			'showFieldsLink' => true,
            			'showGeneralLink' => true,
            			'showLayoutsLink' => true,
            			'showMenusLink' => true,
            	),
            	'globalSearchAttributeNames' => array(
            			'name',
            	),
            
            );
            return $metadata;
        }

        public static function getPrimaryModelName()
        {
            return 'Link';
        }

        
       
        public static function getCreateRight()
        {
            return self::RIGHT_CREATE_LINKS;
        }

        public static function getDeleteRight()
        {
            return self::RIGHT_DELETE_LINKS;
        }
        
        public static function getGlobalSearchFormClassName()
        {
        	return 'LinksSearchForm';
        }
	
		public static function modelsAreNeverGloballySearched()
		{
			return true;
		}
		
		public static function isReportable()
		{
			return true;
		}
    }
?>
