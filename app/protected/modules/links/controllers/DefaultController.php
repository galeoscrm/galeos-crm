<?php
    class LinksDefaultController extends ZurmoModuleController
    {
        public function filters()
        {
            $modelClassName   = $this->getModule()->getPrimaryModelName();
            $viewClassName    = $modelClassName . 'EditAndDetailsView';
            return array_merge(parent::filters(),
                array(
                   array(
                        ZurmoBaseController::REQUIRED_ATTRIBUTES_FILTER_PATH . ' + create, edit',
                        'moduleClassName' => get_class($this->getModule()),
                        'viewClassName'   => $viewClassName,
                   ),
                   array(
                		ZurmoModuleController::ZERO_MODELS_CHECK_FILTER_PATH . ' + list, index',
                		'controller' => $this,
                   ),
               )
            );
        }

        public function actionList($searchFormClassName = null)
        {
            $pageSize = Yii::app()->pagination->resolveActiveForCurrentUserByType(
                            'listPageSize', get_class($this->getModule()));
            $link = new Link(false);
	    if (isset($searchFormClassName) && class_exists($searchFormClassName))
            {
                $searchForm = new $searchFormClassName($link);
                $stickySearchKey = $searchFormClassName; // We need to change this
            }
            else
            {
                $searchForm = new LinksSearchForm($link);
                $stickySearchKey = 'LinksSearchView';
            }

            $listAttributesSelector         = new ListAttributesSelector('LinksListView', get_class($this->getModule()));
            $searchForm->setListAttributesSelector($listAttributesSelector);
            $dataProvider = $this->resolveSearchDataProvider(
                $searchForm,
                $pageSize,
                null,
                'LinksSearchView',
	        $stickySearchKey
            );
            if (isset($_GET['ajax']) && $_GET['ajax'] == 'list-view')
            {
                $mixedView = $this->makeListView(
                    $searchForm,
                    $dataProvider
                );
                $view = new LinksPageView($mixedView);
            }
            else
            {
                $mixedView = $this->makeActionBarSearchAndListView($searchForm, $dataProvider,'SecuredActionBarForLinksSearchAndListView');
                $view = new LinksPageView(ZurmoDefaultViewUtil::
                    makeStandardViewForCurrentUser($this, $mixedView));
            }
            echo $view->render();
        }

        public function actionDetails($id)
        {
        	$link = static::getModelAndCatchNotFoundAndDisplayError('Link', intval($id));
        	ControllerSecurityUtil::resolveAccessCanCurrentUserReadModel($link);
        	AuditEvent::logAuditEvent('ZurmoModule', ZurmoModule::AUDIT_EVENT_ITEM_VIEWED, array(strval($link), 'LinksModule'), $link);
        	$titleBarAndEditView = $this->makeEditAndDetailsView($link, 'Details');
        	$view = new LinksPageView(ZurmoDefaultViewUtil::
        			makeStandardViewForCurrentUser($this, $titleBarAndEditView));
        	echo $view->render();
        }
               
        public function actionCreate()
        {
            $this->actionCreateByModel(new Link());
            
        }

        protected function actionCreateByModel(Link $link, $redirectUrl = null)
        {
            $titleBarAndEditView = $this->makeTitleBarAndEditAndDetailsView(
                                            $this->attemptToSaveModelFromPost($link, $redirectUrl), 'Edit');
            $view = new LinksPageView(ZurmoDefaultViewUtil::
                                         makeStandardViewForCurrentUser($this, $titleBarAndEditView));
            echo $view->render();
        }

        public function actionEdit($id, $redirectUrl = null)
        {
            $link = Link::getById(intval($id));
            ControllerSecurityUtil::resolveAccessCanCurrentUserWriteModel($link);
            $view = new LinksPageView(ZurmoDefaultViewUtil::
                                         makeStandardViewForCurrentUser($this,
                                             $this->makeEditAndDetailsView(
                                                 $this->attemptToSaveModelFromPost($link, $redirectUrl), 'Edit')));
            echo $view->render();
        }

        public function actionModalList()
        {
            $modalListLinkProvider = new SelectFromRelatedEditModalListLinkProvider(
                                            $_GET['modalTransferInformation']['sourceIdFieldId'],
                                            $_GET['modalTransferInformation']['sourceNameFieldId']
            );
            echo ModalSearchListControllerUtil::setAjaxModeAndRenderModalSearchList($this, $modalListLinkProvider,
                                                Yii::t('Default', 'LinksModuleSingularLabel Search',
                                                LabelUtil::getTranslationParamsForAllModules()));
        }

        public function actionDelete($id)
        {
            $link = Link::GetById(intval($id));
            $link->delete();
            $this->redirect(array($this->getId() . '/index'));
        }
        
        public function actionMassDelete()
        {
        	$pageSize = Yii::app()->pagination->resolveActiveForCurrentUserByType(
        			'massDeleteProgressPageSize');
        	$link = new Link(false);
        
        	$activeAttributes = $this->resolveActiveAttributesFromMassDeletePost();
        	$dataProvider = $this->getDataProviderByResolvingSelectAllFromGet(
        			new LinksSearchForm($link),
        			$pageSize,
        			Yii::app()->user->userModel->id,
        			null,
        			'LinksSearchView');
        			$selectedRecordCount = static::getSelectedRecordCountByResolvingSelectAllFromGet($dataProvider);
        			$link = $this->processMassDelete(
        					$pageSize,
        					$activeAttributes,
        					$selectedRecordCount,
        					'LinksPageView',
        					$link,
        					LinksModule::getModuleLabelByTypeAndLanguage('Plural'),
        					$dataProvider
        					);
        			$massDeleteView = $this->makeMassDeleteView(
        					$link,
        					$activeAttributes,
        					$selectedRecordCount,
        					LinksModule::getModuleLabelByTypeAndLanguage('Plural')
        					);
        			$view = new LinksPageView(ZurmoDefaultViewUtil::
        					makeStandardViewForCurrentUser($this, $massDeleteView));
        			echo $view->render();
        }

        /**
         * Override to provide an link specific label for the modal page title.
         * @see ZurmoModuleController::actionSelectFromRelatedList()
         */
        public function actionSelectFromRelatedList($portletId,
                                                    $uniqueLayoutId,
                                                    $relationAttributeName,
                                                    $relationModelId,
                                                    $relationModuleId,
                                                    $pageTitle = null,
                                                    $stateMetadataAdapterClassName = null)
        {
            $pageTitle = Yii::t('Default',
                                'LinksModuleSingularLabel Search',
                                 LabelUtil::getTranslationParamsForAllModules());
            parent::actionSelectFromRelatedList($portletId,
                                                    $uniqueLayoutId,
                                                    $relationAttributeName,
                                                    $relationModelId,
                                                    $relationModuleId,
                                                    $pageTitle);
        }
        
        public function actionCreateFromRelation($relationAttributeName, $relationModelId, $relationModuleId, $redirectUrl)
        {
        	$link             = $this->resolveNewModelByRelationInformation( new Link(),
        			$relationAttributeName,
        			(int)$relationModelId,
        			$relationModuleId);
        
        	$this->actionCreateByModel($link, $redirectUrl);
        }
        
        public function actionExport()
        {
        	$this->export('LinksSearchView');
        }
        
        protected static function getSearchFormClassName()
        {
        	return 'LinksSearchForm';
        }
    }
?>
