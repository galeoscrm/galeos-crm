<?php
    class LinksForAccountRelatedListView extends LinksRelatedListView
    {
        protected function getRelationAttributeName()
        {
            return 'account';
        }

        public static function getDisplayDescription()
        {
            return Zurmo::t('LinksModule', 'LinksModulePluralLabel For AccountsModuleSingularLabel',
                        LabelUtil::getTranslationParamsForAllModules());
        }

        public static function getAllowedOnPortletViewClassNames()
        {
            return array('AccountDetailsAndRelationsView');
        }

        public function renderPortletHeadContent()
        {
            return $this->renderWrapperAndActionElementMenu(Zurmo::t('Core', 'Options'));
        }
    }
?>