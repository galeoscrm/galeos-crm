<?php
   
    abstract class LinksRelatedListView extends SecuredRelatedListView
    {
        public static function getDefaultMetadata()
        {
            $metadata = array(
                'perUser' => array(
                    'title' => "eval:Zurmo::t('LinksModule', 'LinksModulePluralLabel', LabelUtil::getTranslationParamsForAllModules())",
                ),
                'global' => array(
                    'toolbar' => array(
                        'elements' => array(
                            array(  'type'            => 'CreateFromRelatedListLink',
                                    'routeModuleId'   => 'eval:$this->moduleId',
                                    'routeParameters' => 'eval:$this->getCreateLinkRouteParameters()'),
                            array(  'type'            => 'ListByRelatedModelLink',
                                    'routeModuleId'   => 'eval:$this->moduleId',
                                    'routeParameters' => 'eval:$this->getRelatedListLinkRouteParametersForLinks()',
                                    'label' => 'View All Related ' . Zurmo::t('LinksModule', 'LinksModulePluralLabel', LabelUtil::getTranslationParamsForAllModules())),
                        ),
                    ),
                    'rowMenu' => array(
                        'elements' => array(
                            array('type'                      => 'EditLink'),
                            array('type'                      => 'RelatedDeleteLink'),
                        ),
                    ),
                    'gridViewType' => RelatedListView::GRID_VIEW_TYPE_STACKED,
                    'panels' => array(
                        array(
                            'rows' => array(
                            	array('cells' =>
                            		array(
                            			array(
                            				'elements' => array(
                            						array('attributeName' => 'name', 'type' => 'linkurl', 'isLink' => false),
                            				),
                            			),
                            		)
                            	),
                            ),
                        ),
                    ),
                ),
            );
            return $metadata;
        }

        public static function getModuleClassName()
        {
            return 'LinksModule';
        }

        protected function getRelatedListLinkRouteParametersForLinks()
        {
            return array(
                'id'                        => $this->params['relationModel']->id,
                'searchFormClassName'       => 'LinksByRelatedModelsSearchForm',
                'relationAttributeName'     => $this->getRelationAttributeName(),
            );
        }
    }
?>
