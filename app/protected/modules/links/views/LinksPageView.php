<?php
    class LinksPageView extends ZurmoPageView
    {

        protected function getSubtitle()
        {
        	return Zurmo::t('LinksModule', 'LinksModulePluralLabel', LabelUtil::getTranslationParamsForAllModules());
        }
    }
?>