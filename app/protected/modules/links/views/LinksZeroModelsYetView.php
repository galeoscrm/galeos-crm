<?php

    class LinksZeroModelsYetView extends ZeroModelsYetView
    {
        protected function getCreateLinkDisplayLabel()
        {
            return Zurmo::t('LinksModule', 'Create LinksModuleSingularLabel', LabelUtil::getTranslationParamsForAllModules());
        }

        protected function getMessageContent()
        {
            return Zurmo::t('LinksModule', '<h2>"NO LINKs"</h2><i>Please create some</i>');
        }
    }
?>
