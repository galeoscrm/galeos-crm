<?php
    class LinksModalListView extends ModalListView
    {
        public static function getDefaultMetadata()
        {
            $metadata = array(
                'global' => array(
                    'panels' => array(
                        array(
                            'rows' => array(
                            		array('cells' =>
                            				array(
                            						array(
                            								'elements' => array(
                            										array('attributeName' => 'account', 'type' => 'Account', 'isLink' => true),
                            								),
                            						),
                            				)
                            		),
                            		
                                array('cells' =>
                                    array(
                                        array(
                                            'elements' => array(
                                                array('attributeName' => 'name', 'type' => 'Text'),
                                            ),
                                        ),
                                    )
                                ),
                                array('cells' =>
                                    array(
                                        array(
                                            'elements' => array(
                                                array('attributeName' => 'url', 'type' => 'Url'),
                                            ),
                                        ),
                                    )
                                ),
                            ),
                        ),
                    ),
                ),
            );
            return $metadata;
        }
    }
?>