<?php
  
    class SecuredActionBarForLinksSearchAndListView extends SecuredActionBarForSearchAndListView
    {
        /**
         * @return array
         */
        public static function getDefaultMetadata()
        {
        	 $metadata = array(
                'global' => array(
                    'toolbar' => array(
                        'elements' => array(
                            array('type'  => 'CreateMenu',
                                  'iconClass' => 'icon-create'),
                            array('type'  => 'ExportMenu',
                                  'listViewGridId' => 'eval:$this->listViewGridId',
                                  'pageVarName' => 'eval:$this->pageVarName',
                                  'iconClass'   => 'icon-export'),
                            array('type'  => 'MassDeleteMenu',
                                  'listViewGridId' => 'eval:$this->listViewGridId',
                                  'pageVarName' => 'eval:$this->pageVarName',
                                  'iconClass'   => 'icon-delete'),
                        ),
                    ),
                ),
            );
            return $metadata;
        }
    }
?>