<?php
    /**
     * Class used for the dashboard, selectable by users to display a list of their links or filtered any way.
     */
    class LinksMyListView extends SecuredMyListView
    {
        public static function getDefaultMetadata()
        {
            $metadata = array(
                'perUser' => array(
                    'title' => "eval:Zurmo::t('LinksModule', 'My LinksModulePluralLabel', LabelUtil::getTranslationParamsForAllModules())",
                    'searchAttributes' => array('ownedItemsOnly' => true),
                ),
                'global' => array(
                    'panels' => array(
                        array(
                            'rows' => array(
                                array('cells' =>
                                    array(
                                        array(
                                            'elements' => array(
                                                array('attributeName' => 'name', 'type' => 'Text', 'isLink' => true),
                                            ),
                                        ),
                                    )
                                ),
                                array('cells' =>
                                    array(
                                        array(
                                            'elements' => array(
                                                array('attributeName' => 'url', 'type' => 'url'),
                                            ),
                                        ),
                                    )
                                ),
                            ),
                        ),
                    ),
                ),
            );
            return $metadata;
        }

        public static function getModuleClassName()
        {
            return 'LinksModule';
        }

        public static function getDisplayDescription()
        {
            return Zurmo::t('LinksModule', 'My LinksModulePluralLabel', LabelUtil::getTranslationParamsForAllModules());
        }

        protected function getSearchModel()
        {
            $modelClassName = $this->modelClassName;
            return new LinksSearchForm(new $modelClassName(false));
        }

        protected static function getConfigViewClassName()
        {
            return 'LinksMyListConfigView';
        }
    }
?>