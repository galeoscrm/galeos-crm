<?php
    class LinkEditAndDetailsView extends SecuredEditAndDetailsView
    {
        public static function getDefaultMetadata()
        {
            $metadata = array(
                'global' => array(
                    'toolbar' => array(
                        'elements' => array(
                            array('type' => 'CancelLink', 'renderType' => 'Edit'),
                            array('type' => 'SaveButton', 'renderType' => 'Edit'),
                            array('type' => 'ListLink',
                                'renderType' => 'Details',
                                'label' => "eval:Yii::t('Default', 'Return to List')"
                            ),
                            array('type' => 'EditLink', 'renderType' => 'Details'),
                            array('type' => 'AuditEventsModalListLink', 'renderType' => 'Details'),
                        ),
                    ),
                    'derivedAttributeTypes' => array(
                        'DateTimeCreatedUser',
                        'DateTimeModifiedUser',
                    ),
                    'panelsDisplayType' => FormLayout::PANELS_DISPLAY_TYPE_ALL,
                    'panels' => array(
                        array(
                            'rows' => array(
                            	array('cells' =>
                            			array(
                            					array(
                            							'elements' => array(
                            									array('attributeName' => 'account', 'type' => 'Account'),
                            		           		  ),
                            					),
                            			)
								),
                                array('cells' =>
                                    array(
                                        array(
                                            'elements' => array(
                                                array('attributeName' => 'name', 'type' => 'Text'),
                                            ),
                                        ),
                                    )
                                ),
                                array('cells' =>
                                    array(
                                        array(
                                            'elements' => array(
                                                array('attributeName' => 'url', 'type' => 'Url'),
                                            ),
                                        ),
                                    )
                                ),
								array('cells' =>
                            			array(
                            					array(
                            							'detailViewOnly' => true,
                            							'elements' => array(
                            									array('attributeName' => 'null', 'type' => 'DateTimeCreatedUser'),
                            						),
                            					),
                            					array(
                            							'detailViewOnly' => true,
                            							'elements' => array(
                            									array('attributeName' => 'null', 'type' => 'DateTimeModifiedUser'),
                            						),
                            					),
                            			)
                            	),
                            ),
                        ),
                    ),
                ),
            );
            return $metadata;
        }
        
        protected function getNewModelTitleLabel()
        {
        	return Zurmo::t('LinksModule', 'Create LinksModuleSingularLabel',
        			LabelUtil::getTranslationParamsForAllModules());
        }
    }
?>