<?php
    class LinksByRelatedModelsSearchForm extends LinksSearchForm implements ModelByRelatedModelsSearchFormInterface
    {
        public function shouldFilterByRelatedModels()
        {
            return true;
        }

        public function resolveDataProviderClassName()
        {
            return 'RedBeanModelByRelatedModelDataProvider';
        }
    }
?>