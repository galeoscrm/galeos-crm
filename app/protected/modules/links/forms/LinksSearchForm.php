<?php
    class LinksSearchForm extends OwnedSearchForm
    {
    	protected static function getRedBeanModelClassName()
    	{
    		return 'Link';
    	}
    	
    	public function __construct(Link $model)
    	{
    		parent::__construct($model);
    	}
    	
    }
?>