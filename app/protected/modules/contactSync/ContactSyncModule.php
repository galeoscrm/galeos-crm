<?php
    class ContactSyncModule extends Module
    {

    	public function getDependencies()
        {
            return array(
                'zurmo',
            );
        }

        public function getRootModelNames()
        {
            return array('ContactSync');
        }

        public static function getDefaultMetadata()
        {
            $metadata = array();
            return $metadata;
        }

        public static function getPrimaryModelName()
        {
            return 'ContactSync';
        }
        
	
		public static function modelsAreNeverGloballySearched()
		{
			return true;
		}
		
    }
?>
