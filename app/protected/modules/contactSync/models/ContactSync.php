<?php
    class ContactSync extends Item
    {
        public function __toString()
        {
            return $this->id;
        }

        public static function getModuleClassName()
        {
            return 'ContactSyncModule';
        }

        /**
         * Returns the display name for the model class.
         * @return dynamic label name based on module.
         */
        protected static function getLabel($language = null)
        {
            return 'ContactSync';
        }

        /**
         * Returns the display name for plural of the model class.
         * @return dynamic label name based on module.
         */
        protected static function getPluralLabel($language = null)
        {
            return 'ContactsSync';
        }

        public static function canSaveMetadata()
        {
            return true;
        }

        public static function getDefaultMetadata()
        {
            $metadata = parent::getDefaultMetadata();
            $metadata[__CLASS__] = array(
                'members' => array(
                	'processed',
                	'processedTime',
                ),
                'relations' => array(
                	'account'             => array(static::HAS_ONE,   'Account'),
                	'contact'             => array(static::HAS_ONE,   'Contact'),
                ),
                'rules' => array(
                	array('processed',   	  'type',    'type' => 'boolean'),
                	array('processedTime',    'type',    'type' => 'datetime'),
                ),
                'elements' => array(
                	'account'                 => 'Account',
                	'contact'                 => 'Contact',
                ),
                'customFields' => array(
                ),
                'defaultSortAttribute' => 'name',
                'noAudit' => array(
                	'processed',
                	'processedTime',
                ),
            );
            return $metadata;
        }

        public static function isTypeDeletable()
        {
            return true;
        }
        
        public static function getByContact(Contact $contact)
        {
        	assert('$contact->id > 0');
        	$searchAttributeData = array();
        	$searchAttributeData['clauses'] = array(
        			1 => array(
        					'attributeName'        => 'contact',
        					'operatorType'         => 'equals',
        					'value'                => $contact->id
        			),
        	);
        	$searchAttributeData['structure'] = '1';
        	$joinTablesAdapter = new RedBeanModelJoinTablesQueryAdapter('ContactSync');
        	$where             = RedBeanModelDataProvider::makeWhere('ContactSync', $searchAttributeData, $joinTablesAdapter);
        	$result = self::getSubset($joinTablesAdapter, null, null, $where);
        	if ($result == null)
	       		return null;
        	else
        		return $result[0];
        }
    }
?>
