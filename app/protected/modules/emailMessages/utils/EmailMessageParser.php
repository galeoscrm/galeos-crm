<?php
// Helper class to decode attachments

class EmailMessageParser {

	protected $connection;
	protected $messageNumber;

	public $bodyHTML = '';
	public $bodyPlain = '';
	public $attachments;

	public $getAttachments = true;
	public $ignoreInlineAttachments = false;

	public function __construct($connection, $messageNumber) {

		$this->connection = $connection;
		$this->messageNumber = $messageNumber;

	}

	public function fetch() {

		$structure = @imap_fetchstructure($this->connection, $this->messageNumber);
		if(!$structure) {
			return false;
		}
		else {
			if (isset($structure->parts) && count($structure->parts)) {
				$this->recurse($structure->parts);
				return true;
			}
			else {
				return false;
			}
		}

	}

	public function recurse($messageParts, $prefix = '', $index = 1, $fullPrefix = true) {

		foreach($messageParts as $part) {
				
			$partNumber = $prefix . $index;
				
			if($part->type == 0) {
				if($part->subtype == 'PLAIN') {
					$this->bodyPlain .= $this->getPart($partNumber, $part->encoding);
				}
				else {
					$this->bodyHTML .= $this->getPart($partNumber, $part->encoding);
				}
			}
			elseif($part->type == 2) {
				$msg = new EmailMessageParser($this->connection, $this->messageNumber);
				$msg->getAttachments = $this->getAttachments;
				if (isset($part->parts)) {
					$msg->recurse($part->parts, $partNumber.'.', 0, false);
				}

				$this->attachments[] = array(
						'type' => $part->type,
						'subtype' => $part->subtype,
						'filename' => '',
						'attachment' => $msg,
						'inline' => false,
						'is_attachment' => true,
						'name' => '',
				);
			}
			elseif(isset($part->parts)) {
				if($fullPrefix) {
					$this->recurse($part->parts, $prefix.$index.'.');
				}
				else {
					$this->recurse($part->parts, $prefix);
				}
			}
			elseif($part->type > 2) {
				if(isset($part->id) && !$this->ignoreInlineAttachments) {
					$id = str_replace(array('<', '>'), '', $part->id);
					$this->attachments[$id] = array(
							'type' => $part->type,
							'subtype' => $part->subtype,
							'filename' => $this->getFilenameFromPart($part),
							'attachment' => $this->getAttachments ? $this->getPart($partNumber, $part->encoding) : '',
							'inline' => true,
							'is_attachment' => true,
							'name' => '',
					);
				}
				else {
					$this->attachments[] = array(
							'type' => $part->type,
							'subtype' => $part->subtype,
							'filename' => $this->getFilenameFromPart($part),
							'attachment' => $this->getAttachments ? $this->getPart($partNumber, $part->encoding) : '',
							'inline' => false,
							'is_attachment' => true,
							'name' => '',
					);
				}
			}
				
			$index++;
				
		}

	}

	function getPart($partNumber, $encoding) {

		$data = imap_fetchbody($this->connection, $this->messageNumber, $partNumber);
		switch($encoding) {
			case 0: return $data; // 7BIT
			case 1: return $data; // 8BIT
			case 2: return $data; // BINARY
			case 3: return base64_decode($data); // BASE64
			case 4: return quoted_printable_decode($data); // QUOTED_PRINTABLE
			case 5: return $data; // OTHER
		}


	}

	function getFilenameFromPart($part) {

		$filename = '';

		if($part->ifdparameters) {
			foreach($part->dparameters as $object) {
				if(strtolower($object->attribute) == 'filename') {
					$filename = imap_utf8($object->value);
				}
			}
		}

		if(!$filename && $part->ifparameters) {
			foreach($part->parameters as $object) {
				if(strtolower($object->attribute) == 'name') {
					$filename = imap_utf8($object->value);
				}
			}
		}

		return $filename;

	}

}