<?php
    /*********************************************************************************
     * Zurmo is a customer relationship management program developed by
     * Zurmo, Inc. Copyright (C) 2017 Zurmo Inc.
     *
     * Zurmo is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY ZURMO, ZURMO DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Zurmo is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Zurmo, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@zurmo.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Zurmo
     * logo and Zurmo copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Zurmo Inc. 2017. All rights reserved".
     ********************************************************************************/

    /**
     * A class for storing email message contact variable content.
     */
    class EmailMessageContactVariableContent extends Item
    {
        

        public function __toString()
        {
            return Zurmo::t('Core', '(Unnamed)');
        }

        public static function getModuleClassName()
        {
            return 'EmailMessagesModule';
        }

        public static function canSaveMetadata()
        {
            return false;
        }

        public static function getDefaultMetadata()
        {
            $metadata = parent::getDefaultMetadata();
            $metadata[__CLASS__] = array(
                'members' => array(
                    'serializedContent',
                ),
                'relations' => array(
                    'contact'       => array(static::HAS_ONE,  'Contact', static::NOT_OWNED,
                                                static::LINK_TYPE_SPECIFIC, 'contact'),
                    'emailMessage'      => array(static::HAS_ONE,  'EmailMessage', static::NOT_OWNED,
                                                static::LINK_TYPE_SPECIFIC, 'emailMessage'),
                    'campaign'      => array(static::HAS_ONE,  'Campaign', static::NOT_OWNED,
                                                static::LINK_TYPE_SPECIFIC, 'campaign'),
                    'autoresponder'      => array(static::HAS_ONE,  'Autoresponder', static::NOT_OWNED,
                                                static::LINK_TYPE_SPECIFIC, 'autoresponder'),
                ),
                'rules' => array(
                    array('serializedContent',   'type',    'type' => 'string'),
                ),
                'elements' => array(
                    
                ),
            );
            return $metadata;
        }
        
        public static function getByContactIdAndCampaignId($contactId, $campaignId)
        {
            $searchAttributeData = array();
            $searchAttributeData['clauses'] = array(
                1 => array(
                    'attributeName'        => 'contact',
                    'relatedAttributeName' => 'id',
                    'operatorType'         => 'equals',
                    'value'                => $contactId,
                ),
                2 => array(
                    'attributeName'        => 'campaign',
                    'relatedAttributeName' => 'id',
                    'operatorType'         => 'equals',
                    'value'                => $campaignId,
                ),
            );
            $searchAttributeData['structure'] = '1 and 2';
            $joinTablesAdapter = new RedBeanModelJoinTablesQueryAdapter('EmailMessageContactVariableContent');
            $where = RedBeanModelDataProvider::makeWhere('EmailMessageContactVariableContent', $searchAttributeData, $joinTablesAdapter);
            $models = self::getSubset($joinTablesAdapter, null, null, $where, null);
            if (count($models) == 1)
            {
                return $models[0];
            }
            else
            {
                return false;
            }
        }
        
        public static function getByContactIdAndAutoresponderId($contactId, $autoresponderId)
        {
            $searchAttributeData = array();
            $searchAttributeData['clauses'] = array(
                1 => array(
                    'attributeName'        => 'contact',
                    'relatedAttributeName' => 'id',
                    'operatorType'         => 'equals',
                    'value'                => $contactId,
                ),
                2 => array(
                    'attributeName'        => 'autoresponder',
                    'relatedAttributeName' => 'id',
                    'operatorType'         => 'equals',
                    'value'                => $autoresponderId,
                ),
            );
            $searchAttributeData['structure'] = '1 and 2';
            $joinTablesAdapter = new RedBeanModelJoinTablesQueryAdapter('EmailMessageContactVariableContent');
            $where = RedBeanModelDataProvider::makeWhere('EmailMessageContactVariableContent', $searchAttributeData, $joinTablesAdapter);
            $models = self::getSubset($joinTablesAdapter, null, null, $where, null);
            if (count($models) == 1)
            {
                return $models[0];
            }
            else
            {
                return false;
            }
        }
        
        public static function getByEmailMessageId($emailMessageId)
        {
            $searchAttributeData = array();
            $searchAttributeData['clauses'] = array(
                1 => array(
                    'attributeName'        => 'emailMessage',
                    'relatedAttributeName' => 'id',
                    'operatorType'         => 'equals',
                    'value'                => $emailMessageId,
                ),
            );
            $searchAttributeData['structure'] = '1';
            $joinTablesAdapter = new RedBeanModelJoinTablesQueryAdapter('EmailMessageContactVariableContent');
            $where = RedBeanModelDataProvider::makeWhere('EmailMessageContactVariableContent', $searchAttributeData, $joinTablesAdapter);
            $models = self::getSubset($joinTablesAdapter, null, null, $where, null);
            if (count($models) == 1)
            {
                return $models[0];
            }
            else
            {
                return false;
            }
        }
        
        public static function isTypeDeletable()
        {
            return true;
        }

        /**
         * Returns the display name for the model class.
         * @param null | string $language
         * @return dynamic label name based on module.
         */
        protected static function getLabel($language = null)
        {
            return Zurmo::t('CampaignsModule', 'Campaign Email Content', array(), null, $language);
        }

        /**
         * Returns the display name for plural of the model class.
         * @param null | string $language
         * @return dynamic label name based on module.
         */
        protected static function getPluralLabel($language = null)
        {
            return Zurmo::t('CampaignsModule', 'Campaign Email Contents', array(), null, $language);
        }

        protected static function translatedAttributeLabels($language)
        {
            return array_merge(parent::translatedAttributeLabels($language),
                array(
                    'htmlContent' => Zurmo::t('CampaignsModule', 'Html Content',  array(), null, $language),
                    'textContent' => Zurmo::t('CampaignsModule', 'Text Content',  array(), null, $language),
                )
            );
        }
    }
?>