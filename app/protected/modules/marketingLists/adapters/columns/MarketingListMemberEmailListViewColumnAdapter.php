<?php
     /**
     * Helper class to show the marketing list's member contact email in a column in a list view
     */
    class MarketingListMemberEmailListViewColumnAdapter extends EmailListViewColumnAdapter
    {
        public function renderGridViewData()
        {
            $className = get_class($this);
            $value      = $className . '::resolveEmailValue($data->contact, "' . $this->view->getContainerModuleClassName() . '")';
            return array(
                'name'  => 'Email',
                'value' => $value,
                'type'  => 'raw',
            );
        }

        public static function resolveEmailValue($contact, $moduleClassName)
        {
        	return $contact->primaryEmail->emailAddress;
        }
    }
?>