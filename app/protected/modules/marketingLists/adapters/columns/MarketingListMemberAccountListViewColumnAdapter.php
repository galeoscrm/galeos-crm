<?php
     /**
     * Helper class to show the marketing list's Account in a column in a list view
     */
    class MarketingListMemberAccountListViewColumnAdapter extends EmailListViewColumnAdapter
    {
        public function renderGridViewData()
        {
        	$className = get_class($this);
            $value      = $className . '::resolveAccountValue($data->contact, "' . $this->view->getContainerModuleClassName() . '")';
            return array(
                'name'  => 'Account',
                'value' => $value,
                'type'  => 'raw',
            );
        }

        public static function resolveAccountValue($contact, $moduleClassName)
        {
        	return $contact->account->name;
        }
    }
?>