<?php
    /*********************************************************************************
     * Zurmo is a customer relationship management program developed by
     * Zurmo, Inc. Copyright (C) 2017 Zurmo Inc.
     *
     * Zurmo is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY ZURMO, ZURMO DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Zurmo is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Zurmo, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@zurmo.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Zurmo
     * logo and Zurmo copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Zurmo Inc. 2017. All rights reserved".
     ********************************************************************************/

    /**
     * User interface element for managing related model relations for projects. This class supports a MANY_MANY
     * specifically for the 'contacts' relation. This is utilized by the Project model.
     *
     */
    class MultipleContactsForProjectsElement extends MultiSelectRelatedModelsAutoCompleteElement
    {
        protected function getFormName()
        {
            return 'ProjectContactsForm';
        }

        protected function getUnqualifiedNameForIdField()
        {
            return '[contactIds]';
        }

        protected function getUnqualifiedIdForIdField()
        {
            return '_Contact_ids';
        }

        protected function assertModelType()
        {
            assert('$this->model instanceof Project');
        }

        protected function getWidgetSourceUrl()
        {
            return Yii::app()->createUrl('contacts/variableContactState/autoCompleteAllContactsForMultiSelectAutoComplete');
        }

        protected function getWidgetHintText()
        {
            return Zurmo::t('ProjectsModule', 'Type a ' .
                                                LabelUtil::getUncapitalizedModelLabelByCountAndModelClassName(1, 'Contact'),
                                            LabelUtil::getTranslationParamsForAllModules());
        }

        protected function getRelationName()
        {
            return 'contacts';
        }

        protected function getFormattedAttributeLabel()
        {
            return Yii::app()->format->text($this->model->getAttributeLabel('contacts'));
        }

        public static function getDisplayName()
        {
            return Zurmo::t('ContactsModule', 'Related ContactsModulePluralLabel',
                                            LabelUtil::getTranslationParamsForAllModules());
        }

        protected function resolveModelNameForRendering(RedBeanModel $model)
        {
            return ContactsUtil::renderHtmlContentLabelFromContactAndKeyword($model, null);
        }
        
        /**
         * Returns rendered content for display as nonEditable.
         * @return null|string
         */
        protected function renderControlNonEditable()
        {
            $content            = null;
            $existingRecords    = $this->getExistingIdsAndLabelsForDetailsView();
            foreach ($existingRecords as $existingRecord)
            {
                if ($content != null)
                {
                    $content .= ', ';
                }
                $content .= $existingRecord[$this->getWidgetPropertyToSearch()];
            }
            return $content;
        }
        
        /**
         * Returns an array with the Ids and Labels of records already bound to the model attached to element
         * @return array
         */
        protected function getExistingIdsAndLabelsForDetailsView()
        {
            $relatedRecords     = $this->getRelatedRecords();
            $existingRecords    = array();
            $default            = $this->getDefaultExistingIdsAndLabel();
            if (!empty($default))
            {
                $existingRecords[]  = $default;
            }
            foreach ($relatedRecords as $relatedRecord)
            {
                $existingRecord = $this->resolveIdAndNameByModelForDetailsView($relatedRecord);
                if (!empty($existingRecord))
                {
                    $existingRecords[]  = $existingRecord;
                }
            }
            return $existingRecords;
        }
        
        /**
         * Resolve an array with id and name using the sent model
         * @param $model
         * @return array
         */
        protected function resolveIdAndNameByModelForDetailsView(RedBeanModel $model)
        {
            return array(
                'id'    => $model->id,
                $this->getWidgetPropertyToSearch()  => $this->resolveModelNameForRenderingForDetailsView($model),
            );
        }
        
        protected function resolveModelNameForRenderingForDetailsView(RedBeanModel $model)
        {
            return self::renderHtmlContentLabelFromContactAndKeyword($model, null);
        }
        
        /**
         * Given a contact model and a keyword, render the strval of the contact and the matched email address
         * that the keyword matches. If the keyword does not match any email addresses on the contact, render the
         * primary email if it exists. Otherwise just render the strval contact.
         * @param object $contact - model
         * @param string $keyword
         */
        public static function renderHtmlContentLabelFromContactAndKeyword($contact, $keyword)
        {
            assert('$contact instanceof Contact && $contact->id > 0');
            assert('$keyword == null || is_string($keyword)');

            if (substr($contact->secondaryEmail->emailAddress, 0, strlen($keyword)) === $keyword)
            {
                $emailAddressToUse = $contact->secondaryEmail->emailAddress;
            }
            else
            {
                $emailAddressToUse = $contact->primaryEmail->emailAddress;
            }
            if (LeadsUtil::isStateALead($contact->state))
            {
                $moduleDirectory = "leads";
            }
            else
            {
                $moduleDirectory = "contacts";
            }
            if ($emailAddressToUse != null)
            {
                
                $url = Yii::app()->createUrl($moduleDirectory . '/default/details/', array('id' => $contact->id));
                return ZurmoHtml::link(Yii::app()->format->text(strval($contact)). '&#160&#160<b>' . strval($emailAddressToUse) . '</b>',$url);
            }
            else
            {
                $url = Yii::app()->createUrl($moduleDirectory . '/default/details/', array('id' => $contact->id));
                return ZurmoHtml::link(Yii::app()->format->text(strval($contact)),$url);
            }
        }
    }
?>