<?php
    class ContractSearch
    {
        public static function getContractsByLocalContractId($localContractId, $pageSize = null)
        {
            assert('is_string($localContractId)');
            $metadata = array();
            $metadata['clauses'] = array(
                1 => array(
                    'attributeName'        => 'localContractId',
                    'operatorType'         => 'equals',
                    'value'                => $localContractId,
                ),
            );
            $metadata['structure'] = '1';
            $joinTablesAdapter   = new RedBeanModelJoinTablesQueryAdapter('Contract');
            $where  = RedBeanModelDataProvider::makeWhere('Contract', $metadata, $joinTablesAdapter);
            return Contract::getSubset($joinTablesAdapter, null, $pageSize, $where);
        }
        
        public static function getContractsByName($name, $pageSize = null)
        {
        	assert('is_string($localContractId)');
        	$metadata = array();
        	$metadata['clauses'] = array(
        			1 => array(
        					'attributeName'        => 'name',
        					'operatorType'         => 'contains',
        					'value'                => $name
        			),
        	);
        	$metadata['structure'] = '1';
        	$joinTablesAdapter   = new RedBeanModelJoinTablesQueryAdapter('Contract');
        	$where  = RedBeanModelDataProvider::makeWhere('Contract', $metadata, $joinTablesAdapter);
        	return Contract::getSubset($joinTablesAdapter, null, $pageSize, $where);
        }
    }

?>
