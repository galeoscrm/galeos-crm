<?php
    class Contract extends OwnedSecurableItem implements StarredInterface
    {
    	const STATUS_ACTIVE                     = 0;
    	
    	const STATUS_COMPLETED                  = 1;

    	
    	public static function getStatusDropDownArray()
    	{
    		return array(
    				static::STATUS_ACTIVE       => Zurmo::t('ContractsModule', 'Active'),
    				static::STATUS_COMPLETED    => Zurmo::t('ContractsModule', 'Closed'),
    		);
    	}
    	
    	public function __toString()
    	{
    		try
    		{
    			if (trim($this->name) == '')
    			{
    				return Zurmo::t('Core', '(Unnamed)');
    			}
    			return $this->name;
    		}
    		catch (AccessDeniedSecurityException $e)
    		{
    			return '';
    		}
    	}
    	
        public static function getModuleClassName()
        {
            return 'ContractsModule';
        }

        /**
         * Returns the display name for the model class.
         * @return dynamic label name based on module.
         */
        protected static function getLabel($language = null)
        {
            return 'Contract';
        }

        /**
         * Returns the display name for plural of the model class.
         * @return dynamic label name based on module.
         */
        protected static function getPluralLabel($language = null)
        {
            return 'Contracts';
        }

        public static function canSaveMetadata()
        {
            return true;
        }

        public static function getDefaultMetadata()
        {
            $metadata = parent::getDefaultMetadata();
            $metadata[__CLASS__] = array(
                'members' => array(
                	'localContractId',
                	'name',
                	'status',
                	'partnerContractId',
                	'signedBy',
                	'url',
                	'signatureDate',
                	'validUntilDate',
                	'lastRevisionDate',
                	'notes'
                	
                ),
                'relations' => array(
                	'account'                   => array(static::HAS_ONE,   'Account'),
                	'notificationSubscribers'   => array(static::HAS_MANY, 'NotificationSubscriber', static::OWNED,
                				                     static::LINK_TYPE_POLYMORPHIC, 'relatedModel'),
                	'comments'                  => array(static::HAS_MANY, 'Comment', static::OWNED,
                	 		     		  	         static::LINK_TYPE_POLYMORPHIC, 'relatedModel'),
                	'files'                     => array(static::HAS_MANY,  'FileModel', static::OWNED,
                				                     static::LINK_TYPE_POLYMORPHIC, 'relatedModel'),
                ),
                'rules' => array(
                	array('account',     	  'required'),
                	array('localContractId',  'required'),
                	array('localContractId',  'unique'),
                	array('localContractId',  'type',    'type' => 'string'),
                	array('localContractId',  'length',  'max' => 10),
                    array('name',     		  'required'),
                    array('name',     		  'type',    'type' => 'string'),
                    array('name',     		  'length',  'max' => 200),
                	array('status',           'required'),
                	array('status',           'type',    'type' => 'integer'),
                	array('status',           'default', 'value' => static::STATUS_ACTIVE),
                	array('partnerContractId','type',    'type' => 'string'),
                	array('partnerContractId','length',  'max' => 50),
                	array('signedBy',   	  'type',    'type' => 'string'),
                	array('signedBy',  		  'length',  'max' => 200),
                	array('url',              'url',     'defaultScheme' => 'http'),
                	array('signatureDate',    'type',    'type' => 'date'),
                	array('validUntilDate',   'type',    'type' => 'date'),
                	array('lastRevisionDate', 'type',    'type' => 'date'),
                	array('lastRevisionDate', 'dateTimeDefault', 'value' => DateTimeCalculatorUtil::TODAY),
                	array('notes',     		  'type',    'type' => 'string'),
                ),
                'elements' => array(
                	'account'                 => 'Account',
                	'notes'                   => 'TextArea',
                	'signatureDate' 		  => 'Date',
                	'validUntilDate'          => 'Date',
                	'lastRevisionDate'        => 'Date',
                	'status'				  => 'ContractStatusDropDown',
                	'localContractId'		  => 'Text',
                	'files'                   => 'Files',
                ),
                'customFields' => array(
                ),
                'defaultSortAttribute' => 'localContractId',
                'noAudit' => array(
                		'url'
                ),
            	'indexes' => array(
            			'localContractId' => array(
           						'members' => array('localContractId'),
           						'unique' => true),
           		),
            	'labels' => array(
            		'url' => array('en' => 'File URL'),	
            		'localContractId' => array('en' => 'Local Reference'),
            		'partnerContractId' => array('en' => 'Partner Reference'),
            			
            	),
            	
            );
            return $metadata;
        }
        
        public static function isTypeDeletable()
        {
            return true;
        }
        
        public static function hasReadPermissionsOptimization()
        {
        	return true;
        }
        
        public static function getGamificationRulesType()
        {
        	return 'ContractGamification';
        }
        
        public function setNextLocalContractIdNumber()
        {
        	$newLocalContractId = '';
        	
        	$contracts = self::makeModels(ZurmoRedBean::findAll('contract', ' ORDER BY localContractId DESC'));
        	if (count($contracts) > 0){
            	$lastLocalContractId = $contracts[0]->localContractId;
	        	if (substr($lastLocalContractId,0,1) === 'G')
	        	{
	        		$value = substr($lastLocalContractId,1);
	        		if(is_numeric($value) && $value > 0 && $value == round($value, 0))
	        		{
	        			$intvalue = (int)$value + 1;
	        			$newLocalContractId = 'G' . strval($intvalue);
	        		}
	        	}
	        }
        	$this-> localContractId = $newLocalContractId;
        }
        
        protected function beforeSave()
        {
        	if (parent::beforeSave())
        	{
        		$this->resolveAndSetDefaultSubscribers();
        		return true;
        	}
        	else
        	{
        		return false;
        	}
        }
        
        /**
         * Resolve and set default subscribers
         */
        protected function resolveAndSetDefaultSubscribers()
        {
        	NotificationSubscriberUtil::addSubscriber($this->owner, $this, false);
        }
        
        protected static function translatedAttributeLabels($language)
        {
        	$params = LabelUtil::getTranslationParamsForAllModules();
        	return array_merge(parent::translatedAttributeLabels($language),
        			array(
        					'contract'           => Zurmo::t('ContractsModule',      'Parent ContractsModuleSingularLabel',  $params, null, $language),
        					'contracts'          => Zurmo::t('ContractsModule',      'ContractsModulePluralLabel',           $params, null, $language),
        					'status'             => Zurmo::t('ContractsModule',      'Status',                       $params, null, $language),
        					'name'               => Zurmo::t('Core',                 'Name',                         $params, null, $language),
        					'notes'              => Zurmo::t('NotesModule',          'NotesModulePluralLabel',       $params, null, $language),
        					'url'                => Zurmo::t('ContractsModule',      'Url',                          $params, null, $language),
        					'files'              => Zurmo::t('ZurmoModule',          'Files',                        $params, null, $language),
        			)
        	);
        }
    }
?>