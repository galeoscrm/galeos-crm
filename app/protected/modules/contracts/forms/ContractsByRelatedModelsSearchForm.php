<?php
    class ContractsByRelatedModelsSearchForm extends ContractsSearchForm implements ModelByRelatedModelsSearchFormInterface
    {
        public function shouldFilterByRelatedModels()
        {
            return true;
        }

        public function resolveDataProviderClassName()
        {
            return 'RedBeanModelByRelatedModelDataProvider';
        }
    }
?>