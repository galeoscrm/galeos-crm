<?php
    /**
     * Form to all editing and viewing of global configuration for contracts.
     */
    class ContractsConfigurationForm extends ConfigurationForm
    {
        public $useFiles;

        /**
         * Rules for useFiles element in configuration.
         *
         */
        public function rules()
        {
        	return array(
        		array('useFiles',       'required'),
              );
        }

        /**
         * Attribute label name in configuration view.
         */
        public function attributeLabels()
        {
            return array(
                'useFiles' => Zurmo::t('ContractsModule', 'Use Zurmo Files'),
            );
        }
    }
?>