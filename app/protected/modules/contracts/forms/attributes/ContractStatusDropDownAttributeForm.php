<?php

    class ContractStatusDropDownAttributeForm extends HasOneModelAttributeForm
    {
        public static function getAttributeTypeDisplayName()
        {
            return Zurmo::t('ContractsModule', 'Contract');
        }

        public static function getAttributeTypeDisplayDescription()
        {
            return Zurmo::t('ContractsModule', 'An contract field');
        }

        public function getAttributeTypeName()
        {
            return 'Contract';
        }
    }
?>