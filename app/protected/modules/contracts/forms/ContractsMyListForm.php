<?php
    /**
     * Form to all editing and viewing of  configuration for contracts portlet.
     */
    class ContractsMyListForm extends MyListForm
    {
        public $dashboardSize;

        /**
         * Rules for dashboardSize element in configuration.
         *
         */
        public function rules()
        {
        	return array_merge(parent::rules(),
        			array(
        					array('dashboardSize',   'type',    'type' => 'integer'),
        					array('dashboardSize',   'required'),
        					array('dashboardSize',   'numerical', 'min' => 1, 'max' => 100),
        			)
        	);
        }
       
        /**
         * Attribute label name in configuration view.
         */
        public function attributeLabels()
        {
            return array_merge(parent::attributeLabels(), array(
            		'dashboardSize' => Zurmo::t('ContractsModule', 'Size'),
            ));
        }
    }
?>