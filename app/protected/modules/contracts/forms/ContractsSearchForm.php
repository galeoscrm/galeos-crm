<?php
    class ContractsSearchForm extends OwnedSearchForm
    {
    	protected static function getRedBeanModelClassName()
    	{
    		return 'Contract';
    	}
    	
    	public function __construct(Contract $model)
    	{
    		parent::__construct($model);
    	}
    	
    }
?>