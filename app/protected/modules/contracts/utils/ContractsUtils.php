<?php
    class ContractsUtils
    {
        public static function getUseFiles()
        {
            if (null != $useFiles = ZurmoConfigurationUtil::getByModuleName('ContractsModule', 'useFiles'))
            {
                return $useFiles;
            }
            else
            {
                return null;
            }
        }
        
        public static function showFiles()
        {
        	$useFilesArray = ContractsUtils::getUseFiles();
        	if ($useFilesArray != null) {
        		$useFilesInt = current($useFilesArray);
        		if ($useFilesInt == 1) return true;
        	}
        	return false;
        }
    }
   
?>