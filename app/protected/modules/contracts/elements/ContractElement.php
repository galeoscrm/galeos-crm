<?php
    class ContractElement extends ModelElement
    {
        protected static $moduleId = 'contracts';

        
        protected function renderControlEditable()
        {
            assert('$this->model->{$this->attribute} instanceof Contract');
            return parent::renderControlEditable();
        }
    }
?>
