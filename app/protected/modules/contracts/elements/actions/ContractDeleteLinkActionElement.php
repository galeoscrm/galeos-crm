<?php
    class ContractDeleteLinkActionElement extends DeleteLinkActionElement
    {
        protected function resolveConfirmAlertInHtmlOptions($htmlOptions)
        {
            $htmlOptions['confirm'] = Zurmo::t('Core', 'Are you sure you want to delete this {modelLabel}?',
                                      array('{modelLabel}' => ContractsModule::getModuleLabelByTypeAndLanguage('SingularLowerCase')));
            return $htmlOptions;
        }
    }
?>