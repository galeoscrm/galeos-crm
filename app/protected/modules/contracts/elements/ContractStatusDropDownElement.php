<?php
 
    class ContractStatusDropDownElement extends ConstantBasedStaticDropDownFormElement
    {
        protected static $attributeName = 'status';

        protected static function resolveDropDownArray()
        {
            return Contract::getStatusDropDownArray();
        }

        protected function getDropDownArray()
        {
            return Contract::getStatusDropDownArray();
        }
    }
?>