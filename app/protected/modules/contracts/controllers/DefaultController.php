<?php
    class ContractsDefaultController extends ZurmoModuleController
    {
        public function filters()
        {
            $modelClassName   = $this->getModule()->getPrimaryModelName();
            $viewClassName    = $modelClassName . 'EditAndDetailsView';
            return array_merge(parent::filters(),
                array(
                   array(
                        ZurmoBaseController::REQUIRED_ATTRIBUTES_FILTER_PATH . ' + create, createFromRelation, edit',
                        'moduleClassName' => get_class($this->getModule()),
                        'viewClassName'   => $viewClassName,
                   ),
                   array(
                		ZurmoModuleController::ZERO_MODELS_CHECK_FILTER_PATH . ' + list, index',
                		'controller' => $this,
                   ),
              	   array(
                		ZurmoBaseController::RIGHTS_FILTER_PATH . ' + configurationView',
                			'moduleClassName'   => 'ContractsModule',
                			'rightName'         => ContractsModule::RIGHT_ACCESS_CONTRACTS_ADMINISTRATION,
                		),
               )
            );
        }
        
        /**
         * Admin configuration action for entering the parameters.
         */
        public function actionConfigurationView()
        {
        	$breadCrumbLinks = array(
        			Zurmo::t('ContractsModule', 'Contracts Configuration'),
        	);
        	$configurationForm            = new ContractsConfigurationForm();
        	$useFiles = ContractsUtils::getUseFiles();
        	$configurationForm->useFiles  = $useFiles;
        	$postVariableName             = get_class($configurationForm);
        	if (isset($_POST[$postVariableName]))
        	{
        		$configurationForm->setAttributes($_POST[$postVariableName]);
        		if ($configurationForm->validate())
        		{
        			ZurmoConfigurationUtil::setByModuleName('ContractsModule', 'useFiles', $configurationForm->useFiles);
        			
        			Yii::app()->user->setFlash('notification',
        					Zurmo::t('ContractsModule', 'Contracts configuration saved successfully.')
        		    );
        			$this->redirect(Yii::app()->createUrl('contracts/default/configurationView'));
        		}
        	}
        	$editView = new ContractConfigurationView(
        			'Edit',
        			$this->getId(),
        			$this->getModule()->getId(),
        			$configurationForm);
        	$editView->setCssClasses( array('AdministrativeArea') );
        	$view = new ZurmoConfigurationPageView(ZurmoDefaultAdminViewUtil::
        			makeViewWithBreadcrumbsForCurrentUser($this, $editView, $breadCrumbLinks, 'PluginsBreadCrumbView'));
        	echo $view->render();
        }
        
        public function actionList($searchFormClassName = null)
        {
            $pageSize = Yii::app()->pagination->resolveActiveForCurrentUserByType(
                            'listPageSize', get_class($this->getModule()));
            $contract = new Contract(false);
	    	if (isset($searchFormClassName) && class_exists($searchFormClassName))
            {
                $searchForm = new $searchFormClassName($contract);
                $stickySearchKey = $searchFormClassName; // We need to change this
            }
            else
            {
                $searchForm = new ContractsSearchForm($contract);
                $stickySearchKey = 'ContractsSearchView';
            }

            $listAttributesSelector         = new ListAttributesSelector('ContractsListView', get_class($this->getModule()));
            $searchForm->setListAttributesSelector($listAttributesSelector);
            $dataProvider = $this->resolveSearchDataProvider(
                $searchForm,
                $pageSize,
                null,
                'ContractsSearchView',
	        $stickySearchKey
            );
            if (isset($_GET['ajax']) && $_GET['ajax'] == 'list-view')
            {
                $mixedView = $this->makeListView(
                    $searchForm,
                    $dataProvider
                );
                $view = new ContractsPageView($mixedView);
            }
            else
            {
                $mixedView = $this->makeActionBarSearchAndListView($searchForm, $dataProvider,'SecuredActionBarForContractsSearchAndListView');
                $view = new ContractsPageView(ZurmoDefaultViewUtil::
                    makeStandardViewForCurrentUser($this, $mixedView));
            }
            echo $view->render();
        }

        public function actionDetails($id)
        {
        	$contract = static::getModelAndCatchNotFoundAndDisplayError('Contract', intval($id));
        	ControllerSecurityUtil::resolveAccessCanCurrentUserReadModel($contract);
        	AuditEvent::logAuditEvent('ZurmoModule', ZurmoModule::AUDIT_EVENT_ITEM_VIEWED, array(strval($contract), 'ContractsModule'), $contract);
       		$breadCrumbView          = StickySearchUtil::resolveBreadCrumbViewForDetailsControllerAction($this, 'ContractsSearchView', $contract);
       		$detailsAndRelationsView = $this->makeDetailsAndRelationsView($contract, 'ContractsModule',
       				'ContractDetailsAndRelationsView',
       				Yii::app()->request->getRequestUri(),
       				$breadCrumbView);
       		$view                    = new ContractsPageView(ZurmoDefaultViewUtil::
       				makeStandardViewForCurrentUser($this, $detailsAndRelationsView));
        	echo $view->render();
        }
                      
        public function actionCreate()
        {
        	$contract = new Contract();
        	$contract->setNextLocalContractIdNumber();
            $this->actionCreateByModel($contract);
        }

        protected function actionCreateByModel(Contract $contract, $redirectUrl = null)
        {
            $titleBarAndEditView = $this->makeTitleBarAndEditAndDetailsView(
                                            $this->attemptToSaveModelFromPost($contract, $redirectUrl), 'Edit');
            $view = new ContractsPageView(ZurmoDefaultViewUtil::
                                         makeStandardViewForCurrentUser($this, $titleBarAndEditView));
            echo $view->render();
        }

        public function actionEdit($id, $redirectUrl = null)
        {
            $contract = Contract::getById(intval($id));
            ControllerSecurityUtil::resolveAccessCanCurrentUserWriteModel($contract);
            $view = new ContractsPageView(ZurmoDefaultViewUtil::
                                         makeStandardViewForCurrentUser($this,
                                         								$this->makeEditAndDetailsView(
                                         									$this->attemptToSaveModelFromPost($contract, $redirectUrl), 'Edit')));
            echo $view->render();
        }

        public function actionModalList()
        {
            $modalListContractProvider = new SelectFromRelatedEditModalListContractProvider(
                                            $_GET['modalTransferInformation']['sourceIdFieldId'],
                                            $_GET['modalTransferInformation']['sourceNameFieldId']
            );
            echo ModalSearchListControllerUtil::setAjaxModeAndRenderModalSearchList($this, $modalListContractProvider,
                                                Yii::t('Default', 'ContractsModuleSingularLabel Search',
                                                LabelUtil::getTranslationParamsForAllModules()));
        }

        public function actionDelete($id)
        {
            $contract = Contract::GetById(intval($id));
            ControllerSecurityUtil::resolveAccessCanCurrentUserDeleteModel($contract);
            $contract->delete();
            $this->redirect(array($this->getId() . '/index'));
        }

        /**
         * Override to provide an link specific label for the modal page title.
         * @see ZurmoModuleController::actionSelectFromRelatedList()
         */
        public function actionSelectFromRelatedList($portletId,
                                                    $uniqueLayoutId,
                                                    $relationAttributeName,
                                                    $relationModelId,
                                                    $relationModuleId,
                                                    $pageTitle = null,
                                                    $stateMetadataAdapterClassName = null)
        {
            $pageTitle = Yii::t('Default',
                                'ContractsModuleSingularLabel Search',
                                 LabelUtil::getTranslationParamsForAllModules());
            parent::actionSelectFromRelatedList($portletId,
                                                    $uniqueLayoutId,
                                                    $relationAttributeName,
                                                    $relationModelId,
                                                    $relationModuleId,
                                                    $pageTitle);
        }
        
        public function actionCreateFromRelation($relationAttributeName, $relationModelId, $relationModuleId, $redirectUrl)
        {
        	$contract             = $this->resolveNewModelByRelationInformation( new Contract(),
        			$relationAttributeName,
        			(int)$relationModelId,
        			$relationModuleId);

        	$this->actionCreateByModel($contract, $redirectUrl);
        }
        
        public function actionMassDelete()
        {
        	$pageSize = Yii::app()->pagination->resolveActiveForCurrentUserByType(
        			'massDeleteProgressPageSize');
        	$contract = new Contract(false);
        
        	$activeAttributes = $this->resolveActiveAttributesFromMassDeletePost();
        	$dataProvider = $this->getDataProviderByResolvingSelectAllFromGet(
        			new ContractsSearchForm($contract),
        			$pageSize,
        			Yii::app()->user->userModel->id,
        			null,
        			'ContractsSearchView');
        			$selectedRecordCount = static::getSelectedRecordCountByResolvingSelectAllFromGet($dataProvider);
        			$contract = $this->processMassDelete(
        					$pageSize,
        					$activeAttributes,
        					$selectedRecordCount,
        					'ContractsPageView',
        					$contract,
        					ContractsModule::getModuleLabelByTypeAndLanguage('Plural'),
        					$dataProvider
        					);
        			$massDeleteView = $this->makeMassDeleteView(
        					$contract,
        					$activeAttributes,
        					$selectedRecordCount,
        					ContractsModule::getModuleLabelByTypeAndLanguage('Plural')
        					);
        			$view = new ContractsPageView(ZurmoDefaultViewUtil::
        					makeStandardViewForCurrentUser($this, $massDeleteView));
        			echo $view->render();
        }
        
        public function actionMassDeleteProgress()
        {
        	$pageSize = Yii::app()->pagination->resolveActiveForCurrentUserByType(
        			'massDeleteProgressPageSize');
        	$contract = new Contract(false);
        	$dataProvider = $this->getDataProviderByResolvingSelectAllFromGet(
        			new ContractsSearchForm($contract),
        			$pageSize,
        			Yii::app()->user->userModel->id,
        			null,
        			'ContractsSearchView'
        			);
        	$this->processMassDeleteProgress(
        			'Contract',
        			$pageSize,
        			ContractsModule::getModuleLabelByTypeAndLanguage('Plural'),
        			$dataProvider
        			);
        }
        
        public function actionExport()
        {
        	$this->export('ContractsSearchView');
        }
        
        protected static function getSearchFormClassName()
        {
        	return 'ContractsSearchForm';
        }
      
        /**
         * Create comment via ajax for contract
         * @param type $id
         * @param string $uniquePageId
         */
        public function actionInlineCreateCommentFromAjax($id, $uniquePageId)
        {
        	$comment       = new Comment();
        	$redirectUrl   = Yii::app()->createUrl('/contracts/default/inlineCreateCommentFromAjax',
        			array('id'           => $id,
        					'uniquePageId' => $uniquePageId));
        			$urlParameters = array('relatedModelId'           => (int)$id,
        					'relatedModelClassName'    => 'Contract',
        					'relatedModelRelationName' => 'comments',
        					'redirectUrl'              => $redirectUrl); //After save, the url to go to.
        			$uniquePageId  = 'CommentInlineEditForModelView';
        			echo             ZurmoHtml::tag('h2', array(), Zurmo::t('CommentsModule', 'Add Comment'));
        			$inlineView    = new CommentInlineEditView($comment, 'default', 'comments', 'inlineCreateSave',
        					$urlParameters, $uniquePageId);
        			$view          = new AjaxPageView($inlineView);
        			echo $view->render();
        }
        
        /**
         * Add subscriber for contract
         * @param int $id
         */
        public function actionAddSubscriber($id)
        {
        	$contract = Contract::getById((int)$id);
        	$contract    = NotificationSubscriberUtil::processSubscriptionRequest($contract, Yii::app()->user->userModel);
        	$content = NotificationSubscriberUtil::getSubscriberData($contract);
        	$content .= NotificationSubscriberUtil::resolveSubscriptionLink($contract, 'detail-subscribe-model-link', 'detail-unsubscribe-model-link');
        	echo $content;
        }
        
        /**
         * Remove subscriber for contract
         * @param int $id
         */
        public function actionRemoveSubscriber($id)
        {
        	$contract = Contract::getById((int)$id);
        	$contract    = NotificationSubscriberUtil::processUnsubscriptionRequest($contract, Yii::app()->user->userModel);
        	$content = NotificationSubscriberUtil::getSubscriberData($contract);
        	$content .= NotificationSubscriberUtil::resolveSubscriptionLink($contract, 'detail-subscribe-model-link', 'detail-unsubscribe-model-link');
        	if ($content == null)
        	{
        		echo "";
        	}
        	else
        	{
        		echo $content;
        	}
        }
        
        /**
         * Overriding to implement the dedupe action for new contracts
         */
        public function actionSearchForDuplicateModels($attribute, $value)
        {
        	assert('is_string($attribute)');
        	assert('is_string($value)');
        	$model          = new Contract();
        	$dedupeRules    = DedupeRulesFactory::createRulesByModel($model);
        	$viewClassName  = $dedupeRules->getDedupeViewClassName();
        	$searchResult   = $dedupeRules->searchForDuplicateModels($attribute, $value);
        	if ($searchResult != null)
        	{
        		$summaryView    = new $viewClassName($this->id, $this->module->id, $model, $searchResult['matchedModels']);
        		$content        = $summaryView->render();
        		$message        = $searchResult['message'];
        		echo CJSON::encode(array('content' => $content, 'message' => $message));
        	}
        }
        
        protected static function getZurmoControllerUtil()
        {
        	return new ModelHasFilesAndRelatedItemsZurmoControllerUtil('activityItems', 'ActivityItemForm');
        }

    }
?>
