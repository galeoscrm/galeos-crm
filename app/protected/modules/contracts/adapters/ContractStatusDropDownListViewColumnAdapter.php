<?php
    
    class ContractStatusDropDownListViewColumnAdapter extends TextListViewColumnAdapter
    {
        public function renderGridViewData()
        {
            $className  = get_class($this);
            $value      = $className . '::resolveStatusPill($data->status)';
            return array(
                'name'   => 'status',
                'header' => Campaign::getAnAttributeLabel('status'),
                'value' => $value,
                'type'  => 'raw',
            );
        }

        public static function resolveStatusPill($status)
        {
        	$statusString = Contract::getStatusDropDownArray()[$status];
        	
            $label      = CampaignStatusElement::renderNonEditableStringContent((string)$statusString);
            $span       = ZurmoHtml::tag('span', array(), $label);
            $content    = '<i>&#9679;</i>' . $span;
            $class      = 'contract-status ' . strtolower($label);
            $content    = ZurmoHtml::tag('div', compact('class'), $content);
            return $content;
        }
    }
?>
