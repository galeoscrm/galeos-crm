<?php
     class ContracturlListViewColumnAdapter extends TextListViewColumnAdapter
    {
        public function renderGridViewData()
        {
        	if ($this->getIsLink()) {
	        	return array(
	                'name'  => $this->attribute,
	                'value' => 'ZurmoHtml::link($data->name, $data->url, array("target" => "blank","title" => "Open Contract " . $data->url));',
	                'type'  => 'raw',
	            );
        	}
        	else {
        		return array(
        				'name'  => $this->attribute,
        				'value' => 'strval($data->' . $this->attribute . ')',
        				'type'  => 'raw',
        		);
        	}
        }
    }
?>