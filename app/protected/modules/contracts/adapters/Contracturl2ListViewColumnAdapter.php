<?php
     class Contracturl2ListViewColumnAdapter extends TextListViewColumnAdapter
    {
        public function renderGridViewData()
        {
        	return array(
	        	'name'  => '',
	            'value' => 'ZurmoHtml::link("View file", $data->url, array("target" => "blank","title" => "Open Contract " . $data->url));',
	            'type'  => 'raw',
	     	);
    	}
    }
?>