<?php

    /**
     * Base class used for wrapping a view of social items
     */
    class CommentsForContractRelatedModelPortletView extends CommentsForRelatedModelPortletView
    {
        public static function getModuleClassName()
        {
            return 'ContractsModule';
        }

        public static function getAllowedOnPortletViewClassNames()
        {
            return array('ContractDetailsAndRelationsView');
        }
    }
?>