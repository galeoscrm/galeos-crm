<?php
   
    abstract class ContractsRelatedListView extends SecuredRelatedListView
    {
        public static function getDefaultMetadata()
        {
            $metadata = array(
                'perUser' => array(
                    'title' => "eval:Zurmo::t('ContractsModule', 'ContractsModulePluralLabel', LabelUtil::getTranslationParamsForAllModules())",
                ),
                'global' => array(
                    'toolbar' => array(
                        'elements' => array(
                            array(  'type'            => 'CreateFromRelatedListLink',
                                    'routeModuleId'   => 'eval:$this->moduleId',
                                    'routeParameters' => 'eval:$this->getCreateLinkRouteParameters()'),
                            array(  'type'            => 'ListByRelatedModelLink',
                                    'routeModuleId'   => 'eval:$this->moduleId',
                                    'routeParameters' => 'eval:$this->getRelatedListLinkRouteParametersForContracts()',
                                    'label' => 'View All Related ' . Zurmo::t('ContractsModule', 'ContractsModulePluralLabel', LabelUtil::getTranslationParamsForAllModules())),
                        ),
                    ),
                    'rowMenu' => array(
                        'elements' => array(
                            array('type'                      => 'EditLink'),
                            array('type'                      => 'RelatedDeleteLink'),
                        ),
                    ),
                    'gridViewType' => RelatedListView::GRID_VIEW_TYPE_STACKED,
                    'panels' => array(
                        array(
                            'rows' => array(
                            	array('cells' =>
                            			array(
                            					array(
                            							'elements' => array(
                            									array('attributeName' => 'localContractId', 'type' => 'text', 'isLink' => true),
                            							),
                            					),
                            			)
                            	),
                           		array('cells' =>
                        				array(
                          						array(
                          								'elements' => array(
                           										array('attributeName' => 'name', 'type' => 'Text', 'isLink' => false),
                           								),
                           						),
                           				)
								),
                            	array('cells' =>
                            		array(
                            			array(
                            				'elements' => array(
                            						array('attributeName' => 'name', 'type' => 'contracturl2', 'isLink' => true),
                            				),
                            			),
                            		)
                            	),
                            ),
                        ),
                    ),
                ),
            );
            return $metadata;
        }

        public static function getModuleClassName()
        {
            return 'ContractsModule';
        }

        protected function getRelatedListLinkRouteParametersForContracts()
        {
            return array(
                'id'                        => $this->params['relationModel']->id,
                'searchFormClassName'       => 'ContractsByRelatedModelsSearchForm',
                'relationAttributeName'     => $this->getRelationAttributeName(),
            );
        }
    }
?>