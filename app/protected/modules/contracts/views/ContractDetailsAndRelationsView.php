<?php

    class ContractDetailsAndRelationsView extends ConfigurableDetailsAndRelationsView
    {
        public function isUniqueToAPage()
        {
            return true;
        }

        public static function getDefaultMetadata()
        {
            $metadata = array(
                'global' => array(
                    'toolbar' => array(
                        'elements' => array(
                            array(  'type'           => 'AddPortletAjaxLinkOnDetailView',
                                    'uniqueLayoutId' => 'eval:$this->uniqueLayoutId',
                                    'ajaxOptions'    => 'eval:static::resolveAjaxOptionsForAddPortlet()',
                                    'htmlOptions'    => array('id' => 'AddPortletLink',
                                    //'class'          => 'icon-add'
                                )
                            ),
                        ),
                    ),
                    'columns' => array(
                        array(
                            'rows' => array(
                               array(
                                    'type' => 'ContractDetailsPortlet',
                                ),
                               array(
                                    'type' => 'CommentsForContractRelatedModelPortlet',
                                ),
                            )
                        ),
                        
                    )
                )
            );
            return $metadata;
        }

        /**
         * Renders content.
         * @return string
         */
        protected function renderContent()
        {
            $content = parent::renderContent();
            TasksUtil::resolveShouldOpenToTaskForDetailsAndRelationsView();
            return $content;
        }
    }
?>