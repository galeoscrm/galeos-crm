<?php
    /**
     * Layout for the business card view for an contract.
     */
    class ContractCardViewLayout extends CardViewLayout
    {
        public function __construct($model, $haveGoToDetailsLink = false)
        {
            assert('$model instanceof Contract');
            assert('is_bool($haveGoToDetailsLink)');
            $this->model               = $model;
            $this->haveGoToDetailsLink = $haveGoToDetailsLink;
        }

        protected function renderFrontOfCardContent()
        {
            $content = $this->resolveNameContent();
            return $content;
        }
        
        protected function resolveNameContent()
        {
        	$element                       = new TextElement($this->model, 'localContractId', null);
        	$element->nonEditableTemplate  = '{content}';
        	$starLink                      = null;
        	$spanContent                   = null;
        	if (StarredUtil::modelHasStarredInterface($this->model))
        	{
        		$starLink = StarredUtil::getToggleStarStatusLink($this->model, null);
        	}
        	$localContractId               = $element->render();
        	if ($localContractId != null)
        	{
        		$spanContent = ZurmoHtml::tag('span', array(), $element->render());
        	}
        	return ZurmoHtml::tag('h2', array(), $spanContent . strval($this->model) . $starLink . $this->renderGoToDetailsLink());
        }
       
        protected function renderGoToDetailsLink()
        {
            if ($this->haveGoToDetailsLink)
            {
                $link = Yii::app()->createUrl('contracts/default/details/', array('id' => $this->model->id));
                return ZurmoHtml::link(Zurmo::t('ZurmoModule', 'Go To Details'), $link, array('class' => 'simple-link', 'target' => '_blank'));
            }
        }

        protected function renderBackOfCardContent()
        {
            return null;
        }
    }
?>
