<?php
class ContractsToMergeListAndChartView extends CreateModelsToMergeListAndChartView
{
	protected function renderDetailsViewForDupeModel($model)
	{
		$content = null;
		$layout  = new ContractCardViewLayout($model, true);
		$content  = $layout->renderContent();
		return $content;
	}
}