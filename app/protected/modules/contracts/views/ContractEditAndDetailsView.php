<?php
    class ContractEditAndDetailsView extends SecuredEditAndDetailsView
    {
    	protected $viewContainsFileUploadElement = true;
    	protected static $showFiles;
    	 
        public static function getDefaultMetadata()
        {
        	$showFiles = ContractsUtils::showFiles();
        	 
        	$rows = array(
        			array('cells' =>
        					array(
        							array(
        									'elements' => array(
        											array('attributeName' => 'localContractId', 'type' => 'Text'),
        									),
        							),
        					)
        			),
        			array('cells' =>
        					array(
        							array(
        									'elements' => array(
        											array('attributeName' => 'name', 'type' => 'Text'),
        									),
        							),
        					)
        			),
        			array('cells' =>
        					array(
        							array(
        									'elements' => array(
        											array('attributeName' => 'account', 'type' => 'Account'),
        									),
        							),
        					)
        			),
        			array('cells' =>
        					array(
        							array(
        									'elements' => array(
        											array('attributeName' => 'partnerContractId', 'type' => 'Text'),
        									),
        							),
        					)
        			),
        			array('cells' =>
        					array(
        							array(
        									'elements' => array(
        											array('attributeName' => 'signedBy', 'type' => 'Text'),
        									),
        							),
        					)
        			),
        			array('cells' =>
        					array(
        							array(
        									'elements' => array(
        											array('attributeName' => 'signatureDate', 'type' => 'Date'),
        									),
        							),
        					)
        			),
        			array('cells' =>
        					array(
        							array(
        									'elements' => array(
        											array('attributeName' => 'validUntilDate', 'type' => 'Date'),
        									),
        							),
        					)
        			),
        			array('cells' =>
        					array(
        							array(
        									'elements' => array(
        											array('attributeName' => 'lastRevisionDate', 'type' => 'Date'),
        									),
        							),
        					)
        			),
        			array('cells' =>
        					array(
        							array(
        									'elements' => array(
        											array('attributeName' => 'status', 'type' => 'ContractStatusDropDown', 'addBlank' => true),
        									),
        							),
        					)
        			),
        			array('cells' =>
        					array(
        							array(
        									'elements' => array(
        											array('attributeName' => 'url', 'type' => 'Url'),
        									),
        							),
        					)
        			),
        			array('cells' =>
        					array(
        							array(
        									'elements' => array(
        											array('attributeName' => 'notes', 'type' => 'TextArea'),
        									),
        							),
        					)
        			),
        			array('cells' =>
        					array(
        							array(
        									'detailViewOnly' => true,
        									'elements' => array(
        											array('attributeName' => 'null', 'type' => 'DateTimeCreatedUser'),
        									),
        							),
        							array(
        									'detailViewOnly' => true,
        									'elements' => array(
        											array('attributeName' => 'null', 'type' => 'DateTimeModifiedUser'),
        									),
        							),
        					)
        			),
        	);
        
        	if ($showFiles)
        	{
        		$filecell = array('cells' =>
        				array(
        						array(
        								'elements' => array(
        										array('attributeName' => 'null', 'type' => 'Files',
        												'showMaxSize'   => false),
        								),
        						),
        				)
        		);
        		array_push($rows, $filecell);
        	}
        	
            $metadata = array(
                'global' => array(
                    'toolbar' => array(
                        'elements' => array(
                            array('type' => 'CancelLink', 'renderType' => 'Edit'),
                            array('type' => 'SaveButton', 'renderType' => 'Edit'),
                            array('type' => 'ListLink',
                                'renderType' => 'Details',
                                'label' => "eval:Yii::t('Default', 'Return to List')"
                            ),
                            array('type' => 'EditLink', 'renderType' => 'Details'),
                        	array('type' => 'ContractDeleteLink', 'renderType' => 'Details'),
                            array('type' => 'AuditEventsModalListLink', 'renderType' => 'Details'),
                        ),
                    ),
                    'derivedAttributeTypes' => array(
                        'DateTimeCreatedUser',
                        'DateTimeModifiedUser',
                    	'Files'
                    ),
                    'panelsDisplayType' => FormLayout::PANELS_DISPLAY_TYPE_ALL,
                    'panels' => array(
                        array('rows' => $rows ),
                    ),
                ),
            );
            
           
            
            return $metadata;
        }
        
        protected function getNewModelTitleLabel()
        {
        	return Zurmo::t('ContractsModule', 'Create ContractsModuleSingularLabel',
        			LabelUtil::getTranslationParamsForAllModules());
        }
        
        public static function getDesignerRulesType()
        {
        	return 'DetailsViewOnlyForUserOwnerEditAndDetailsView';
        }
    }
?>