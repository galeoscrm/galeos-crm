<?php
    class ContractsPageView extends ZurmoPageView
    {

        protected function getSubtitle()
        {
        	return Zurmo::t('ContractsModule', 'ContractsModulePluralLabel', LabelUtil::getTranslationParamsForAllModules());
        }
    }
?>