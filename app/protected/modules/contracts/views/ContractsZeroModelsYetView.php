<?php

    class ContractsZeroModelsYetView extends ZeroModelsYetView
    {
        protected function getCreateLinkDisplayLabel()
        {
            return Zurmo::t('ContractsModule', 'Create ContractsModuleSingularLabel', LabelUtil::getTranslationParamsForAllModules());
        }

        protected function getMessageContent()
        {
            return Zurmo::t('ContractsModule', '<h2>"NO CONTRACTs"</h2><i>Please create some</i>');
        }
    }
?>
