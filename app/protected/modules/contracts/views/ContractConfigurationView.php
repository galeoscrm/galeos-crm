<?php

    /**
     * Edit and details view for the admin configuration view for contracts.
     */
    class ContractConfigurationView extends EditAndDetailsView
    {
        public function getTitle()
        {
            return Zurmo::t('ContractsModule', 'Contracts Configuration');
        }

        public static function getDefaultMetadata()
        {
            $metadata = array(
                'global' => array(
                    'toolbar' => array(
                        'elements' => array(
                            array('type' => 'SaveButton',        'renderType' => 'Edit'),
                            array('type' => 'ConfigurationLink', 'label' => "eval:Zurmo::t('Core', 'Cancel')"),
                            array('type' => 'EditLink',          'renderType' => 'Details'),
                        ),
                    ),
                    'panelsDisplayType' => FormLayout::PANELS_DISPLAY_TYPE_ALL,
                    'panels' => array(
                        array(
                            'rows' => array(
                               array('cells' =>
                                    array(
                                        array(
                                            'elements' => array(
                                                array('attributeName' => 'useFiles',
                                                      'type' => 'BooleanStaticDropDown',
                                                ),
                                             ),
                                        ),
                                    )
                               ),
                            ),
                        ),
                    ),
                ),
            );
            return $metadata;
        }

        protected function getNewModelTitleLabel()
        {
            return null;
        }
        
        public static function getDesignerRulesType()
        {
        	return 'DetailsView';
        }
    }
?>