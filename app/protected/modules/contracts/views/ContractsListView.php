<?php
    class ContractsListView extends SecuredListView
    {
    	
        public static function getDefaultMetadata()
        {
            $metadata = array(
                'global' => array(
                    'panels' => array(
                        array(
                            'rows' => array(
                            	array('cells' =>
                            			array(
                            					array(
                            							'elements' => array(
                            									array('attributeName' => 'localContractId', 'type' => 'Text', 'isLink' => true),
                            							),
                            					),
                            			)
                            	),
                            	array('cells' =>
                            		array(
                            				array(
                            						'elements' => array(
                            								array('attributeName' => 'account', 'type' => 'Account', 'isLink' => true),
                            						),
                            				),
                            		)
                            	),
                  
                                array('cells' =>
                                    array(
                                        array(
                                            'elements' => array(
                                                array('attributeName' => 'name', 'type' => 'Text', 'isLink' => false),
                                            ),
                                        ),
                                    )
                                ),
                            	array('cells' =>
                            			array(
                            					array(
                            							'elements' => array(
                            									array('attributeName' => 'signatureDate', 'type' => 'Date'),
                            							),
                            					),
                            			)
                            	),
                            	array('cells' =>
                            			array(
                            					array(
                            							'elements' => array(
                            									array('attributeName' => 'lastRevisionDate', 'type' => 'Date'),
                            							),
                            					),
                            			)
                            	),
                            	array('cells' =>
                            			array(
                            					array(
                            							'elements' => array(
                            									array('attributeName' => 'validUntilDate', 'type' => 'Date'),
                            							),
                            					),
                            			)
                            	),
                                array('cells' =>
                                    array(
                                        array(
                                            'elements' => array(
                                                array('attributeName' => 'status', 'type' => 'ContractStatusDropDown'),
                                            ),
                                        ),
                                    )
                                ),  	
                            ),
                        ),
                    ),
                ),
            );
            return $metadata;
        }
        
        protected function getCGridViewColumns()
        {
        	$columns = parent::getCGridViewColumns();
        	$lastColumn = $this->getCGridViewShowContractColumn();
           	if (!empty($lastColumn))
        	{
        		array_push($columns, $lastColumn);
        	}
        	return $columns;
        }
        
        protected function getCGridViewShowContractColumn()
        {
        	$url  = '$data->url';
            return array(
                'class'           => 'ButtonColumn',
                'template'        => '{update}',
                'buttons' => array(
                    'update' => array(
                    'url' => $url,
                    'imageUrl'        => false,
                    'options'         => array('class' => 'pencil', 'title' => 'Show Contract'),
                    'label'           => 'z'
                    ),
                ),
            );
        }
    }
?>