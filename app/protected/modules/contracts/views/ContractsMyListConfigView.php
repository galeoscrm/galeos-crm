<?php
    /**
     * View for showing the configuration parameters for the @see ContractsMyListView.
     */
    class ContractsMyListConfigView extends MyListConfigView
    {
        public static function getDefaultMetadata()
        {
            $metadata = array(
                'global' => array(
                    'toolbar' => array(
                        'elements' => array(
                            array('type' => 'SaveButton'),
                        ),
                    ),
                    'nonPlaceableAttributeNames' => array(
                        'anyMixedAttributes',
                    ),
                    'panelsDisplayType' => FormLayout::PANELS_DISPLAY_TYPE_ALL,
                    'panels' => array(
                        array(
                            'title' => 'List Filters',
                            'rows' => array(
                                array('cells' =>
                                    array(
                                        array(
                                            'elements' => array(
                                                array('attributeName' => 'ownedItemsOnly', 'type' => 'CheckBox'),
                                            ),
                                        ),
                                    )
                                ),
                            	array('cells' =>
                            		array(
                            			array(
                            				'elements' => array(
                            					array('attributeName' => 'status', 'type' => 'ContractStatusDropDown'),
                            				),
                            			),
                            		)
                            	),
                            ),
                        ),
                    ),
                ),
            );
            return $metadata;
        }

        public static function getDisplayDescription()
        {
            return Zurmo::t('ContractsModule', 'My ContractsModulePluralLabel', LabelUtil::getTranslationParamsForAllModules());
        }

        public static function getModelForMetadataClassName()
        {
            return 'ContractsSearchForm';
        }
        
        protected function afterResolveMetadataWithRenderedElements(& $metadataWithRenderedElements, $form)
        {
        	assert('is_array($metadataWithRenderedElements)');
        	assert('$form == null || $form instanceof ZurmoActiveForm');
        	$element             = new IntegerElement($this->model, 'dashboardSize', $form);
        	$dashboardSizeData   = array();
        	$dashboardSizeData['rows'][0]['cells'][0]['elements'][0] = $element->render();
        	array_unshift($metadataWithRenderedElements['global']['panels'], $dashboardSizeData);
        	parent::afterResolveMetadataWithRenderedElements($metadataWithRenderedElements, $form);   	
        }
    }
?>