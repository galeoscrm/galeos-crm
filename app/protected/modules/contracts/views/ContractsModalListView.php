<?php
    class ContractsModalListView extends ModalListView
    {
    	public static function getDefaultMetadata()
    	{
    		$metadata = array(
    				'global' => array(
    						'panels' => array(
    								array(
    										'rows' => array(
    												array('cells' =>
    														array(
    																array(
    																		'elements' => array(
    																				array('attributeName' => 'localContractId', 'type' => 'Text','isLink' => true),
    																		),
    																),
    														)
    												),
    												array('cells' =>
    														array(
    																array(
    																		'elements' => array(
    																				array('attributeName' => 'account', 'type' => 'Account', 'isLink' => false),
    																		),
    																),
    														)
    												),
    	
    												array('cells' =>
    														array(
    																array(
    																		'elements' => array(
    																				array('attributeName' => 'name', 'type' => 'Text','isLink' => false),
    																		),
    																),
    														)
    												),
    												array('cells' =>
    														array(
    																array(
    																		'elements' => array(
    																				array('attributeName' => 'signatureDate', 'type' => 'Date'),
    																		),
    																),
    														)
    												),
    												array('cells' =>
    														array(
    																array(
    																		'elements' => array(
    																				array('attributeName' => 'lastRevisionDate', 'type' => 'Date'),
    																		),
    																),
    														)
    												),
    	
    												array('cells' =>
    														array(
    																array(
    																		'elements' => array(
    																			array('attributeName' => 'status', 'type' => 'ContractStatusDropDown'),
    																		),
    																),
    														)
    												),
    												array('cells' =>
    														array(
    																array(
    																		'elements' => array(
    																				array('attributeName' => 'name', 'type' => 'contracturl2'),
    																		),
    																),
    														)
    												),
    										),
    								),
    						),
    				),
    	
    		);
    		return $metadata;
    	}
    }
?>