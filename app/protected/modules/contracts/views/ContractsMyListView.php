<?php
    /**
     * Class used for the dashboard, selectable by users to display a list of their links or filtered any way.
     */
    class ContractsMyListView extends SecuredMyListView
    {
        public static function getDefaultMetadata()
        {
            $metadata = array(
                'perUser' => array(
                    'title' => "eval:Zurmo::t('ContractsModule', 'My ContractsModulePluralLabel', LabelUtil::getTranslationParamsForAllModules())",
                    'searchAttributes' => array('ownedItemsOnly' => true),
                ),
            		'global' => array(
            				'panels' => array(
            						array(
            								'rows' => array(
            										array('cells' =>
            												array(
            														array(
            																'elements' => array(
            																		array('attributeName' => 'localContractId', 'type' => 'Text','isLink' => true),
            																),
            														),
            												)
            										),
            										array('cells' =>
            												array(
            														array(
            																'elements' => array(
            																		array('attributeName' => 'account', 'type' => 'Account', 'isLink' => true),
            																),
            														),
            												)
            										),
            		
            										array('cells' =>
            												array(
            														array(
            																'elements' => array(
            																		array('attributeName' => 'name', 'type' => 'Text','isLink' => false),
            																),
            														),
            												)
            										),
            										array('cells' =>
            												array(
            														array(
            																'elements' => array(
            																		array('attributeName' => 'lastRevisionDate', 'type' => 'Date'),
            																),
            														),
            												)
            										),
            		
            										array('cells' =>
            												array(
            														array(
            																'elements' => array(
            																	array('attributeName' => 'status', 'type' => 'ContractStatusDropDown'),
            																),
            														),
            												)
            										),            										
            								),
            						),
            				),
            		),
            );
            return $metadata;
        }

        public static function getModuleClassName()
        {
            return 'ContractsModule';
        }

        public static function getDisplayDescription()
        {
            return Zurmo::t('ContractsModule', 'My ContractsModulePluralLabel', LabelUtil::getTranslationParamsForAllModules());
        }

        protected function getSearchModel()
        {
            $modelClassName = $this->modelClassName;
            return new ContractsSearchForm(new $modelClassName(false));
        }

        protected static function getConfigViewClassName()
        {
            return 'ContractsMyListConfigView';
        }
        
        protected function getCGridViewColumns()
        {
        	$columns = parent::getCGridViewColumns();
        	$lastColumn = $this->getCGridViewShowContractColumn();
        	if (!empty($lastColumn))
        	{
        		array_push($columns, $lastColumn);
        	}
        	return $columns;
        }
        
        protected function getCGridViewShowContractColumn()
        {
        	$url  = '$data->url';
        	return array(
        			'class'           => 'ButtonColumn',
        			'template'        => '{update}',
        			'buttons' => array(
        					'update' => array(
        							'url' => $url,
        							'imageUrl'        => false,
        							'options'         => array('class' => 'pencil', 'title' => 'Show Contract'),
        							'label'           => 'z'
        					),
        			),
        	);
        }
        
        protected function getSortAttributeForDataProvider()
        {
            return 'localContractId';
        }

        protected function resolveSortDescendingForDataProvider()
        {
            return true;
        }
        
        protected function makeDataProviderBySearchAttributeData($searchAttributeData)
        {
        	assert('is_array($searchAttributeData)');
        	list($sortAttribute, $sortDescending)  =
        	SearchUtil::resolveSortFromStickyData($this->modelClassName, $this->uniqueLayoutId);
        	$pageSize = Yii::app()->pagination->resolveActiveForCurrentUserByType('dashboardListPageSize');
        	$pageSize = $this->getDashboardSize();
        	
        	if ($sortAttribute === null)
        	{
        		$sortAttribute  = $this->getSortAttributeForDataProvider();
        		$sortDescending = $this->resolveSortDescendingForDataProvider();
        	}
        	$dataProviderClassName = $this->getDataProviderClassName();
        	$redBeanModelDataProvider = new $dataProviderClassName($this->modelClassName, $sortAttribute, $sortDescending,
        			$searchAttributeData, array(
        					'pagination' => array(
        							'pageSize' => $pageSize,
        					)
        			));
        	$sort                     = new RedBeanSort($redBeanModelDataProvider->modelClassName);
        	$sort->sortVar            = $redBeanModelDataProvider->getId().'_sort';
        	$sort->route              = 'defaultPortlet/myListDetails';
        	$sort->params             = array_merge(GetUtil::getData(), array('portletId' => $this->params['portletId']));
        	$redBeanModelDataProvider->setSort($sort);
        	return $redBeanModelDataProvider;
        }
        
        public function getConfigurationView()
        {
        	$searchForm   = $this->getSearchModel();
        	$searchForm->getModel()->setScenario('searchModel');
        	$formModel    = new ContractsMyListForm();
        	if ($this->viewData != null)
        	{
        		if (isset($this->viewData['searchAttributes']))
        		{
        			$searchForm->setAttributes($this->viewData['searchAttributes']);
        		}
        		if (isset($this->viewData['title']))
        		{
        			$formModel->setAttributes(array('title' => $this->viewData['title']));
        		}
        		
        		$formModel->setAttributes(array('dashboardSize' => $this->getDashboardSize()));
        		
        	}
        	else
        	{
        		$searchForm->setAttributes(static::getDefaultSearchAttributes());
        		$formModel->setAttributes(
        				array(
        						'title' => static::getDefaultTitle(),
        						'dashboardSize' => static::getDefaultDashboardSize()
        				)
        		);
        	}
        	$configViewClassName = static::getConfigViewClassName();
        	return new $configViewClassName($formModel, $searchForm, $this->params);
        }
        
        public static function getDefaultDashboardSize()
        {
        	return 10;
        }
        
        public function getDashboardSize()
        {
        	if (!empty($this->viewData['dashboardSize']))
        	{
        		return $this->viewData['dashboardSize'];
        	}
        	else
        	{
        		return static::getDefaultDashboardSize();
        	}
        }
    }
?>