<?php
    class ContractsModule extends SecurableModule
    {
        const RIGHT_CREATE_CONTRACTS = 'Create Contracts';
        const RIGHT_DELETE_CONTRACTS = 'Delete Contracts';
        const RIGHT_ACCESS_CONTRACTS = 'Access Contracts Tab';
        const RIGHT_ACCESS_CONTRACTS_ADMINISTRATION = "Access Contracts Administration";

        public function getDependencies()
        {
            return array(
             	'configuration',
                'zurmo',
            );
        }

        public function getRootModelNames()
        {
            return array('Contract');
        }
        
        public static function getTranslatedRightsLabels()
        {
        	$params                              = LabelUtil::getTranslationParamsForAllModules();
        	$labels                              = array();
        	$labels[self::RIGHT_CREATE_CONTRACTS] = Zurmo::t('ContractsModule', 'Create ContractsModulePluralLabel', $params);
        	$labels[self::RIGHT_DELETE_CONTRACTS] = Zurmo::t('ContractsModule', 'Delete ContractsModulePluralLabel', $params);
        	$labels[self::RIGHT_ACCESS_CONTRACTS] = Zurmo::t('ContractsModule', 'Access ContractsModulePluralLabel Tab', $params);
        	$labels[self::RIGHT_ACCESS_CONTRACTS_ADMINISTRATION] = Zurmo::t('ContractsModule', 'Access ContractsModulePluralLabel Administration', $params);
        	return $labels;
        }

        public static function getDefaultMetadata()
        {
            $metadata = array();
            $metadata['global'] = array(
           		'configureSubMenuItems' => array(
           				array(
           					'category'         => ZurmoModule::ADMINISTRATION_CATEGORY_PLUGINS,
        					'titleLabel'       => "eval:Zurmo::t('ContractsModule', 'ContractsModulePluralLabel', \$translationParams)",
           					'descriptionLabel' => "eval:Zurmo::t('ContractsModule', 'Manage ContractsModulePluralLabel Configuration', \$translationParams)",
           					'route'            => '/contracts/default/configurationView',
           					'right'            => self::RIGHT_ACCESS_CONTRACTS_ADMINISTRATION,
           				),
            	),
            	'designerMenuItems' => array(
            			'showFieldsLink' => true,
            			'showGeneralLink' => true,
            			'showLayoutsLink' => true,
            			'showMenusLink' => true,
            	),
            	'globalSearchAttributeNames' => array(
            			'name',
            			'localContractId',
            			'partnerContractId',
            	),
            	'startingState'=> 1,
            	'tabMenuItems' => array(
            			array(
            					'label'  => "eval:Zurmo::t('ContractsModule', 'ContractsModulePluralLabel', \$translationParams)",
            					'url'    => array('/contracts/default'),
            					'right'  => self::RIGHT_ACCESS_CONTRACTS,
            					'mobile' => true,
             			),
            	),
				'shortcutsCreateMenuItems' => array(
            			array(
            					'label'  => "eval:Zurmo::t('ContractsModule', 'ContractsModuleSingularLabel', \$translationParams)",
            					'url'    => array('/contracts/default/create'),
            					'right'  => self::RIGHT_CREATE_CONTRACTS,
            					'mobile' => true,
            			),
            	),
            );
            
            return $metadata;
        }

        public static function getPrimaryModelName()
        {
            return 'Contract';
        }
        
        public static function getAccessRight()
        {
            return self::RIGHT_ACCESS_CONTRACTS;
        }

        public static function getCreateRight()
        {
            return self::RIGHT_CREATE_CONTRACTS;
        }

        public static function getDeleteRight()
        {
            return self::RIGHT_DELETE_CONTRACTS;
        }
        
        public static function getGlobalSearchFormClassName()
        {
        	return 'ContractsSearchForm';
        }
	
		public static function modelsAreNeverGloballySearched()
		{
			return false;
		}
		
		public static function hasPermissions()
		{
			return true;
		}
		
		public static function isReportable()
		{
			return true;
		}
		
    }
?>
