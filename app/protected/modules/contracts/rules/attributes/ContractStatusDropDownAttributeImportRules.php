<?php
    /**
     * Import rules for the contact status attribute. This is used for the states that are the starting state or after.
    */
    class ContractStatusDropDownAttributeImportRules extends DerivedAttributeImportRules
    {
    
    	public function getDisplayLabel()
    	{
    		return Zurmo::t('ZurmoModule', 'Status');
    	}
    
    	/**
    	 * @return array
    	 */
    	public function getRealModelAttributeNames()
    	{
    		return array('status');
    	}
    
    	public static function getSanitizerUtilTypesInProcessingOrder()
    	{
    		return array();
    	}
    
    	public function resolveValueForImport($value, $columnName, $columnMappingData, ImportSanitizeResultsUtil $importSanitizeResultsUtil)
    	{
    		assert('is_string($columnName)');
    		$attributeNames = $this->getRealModelAttributeNames();
    		assert('count($attributeNames) == 1');
    		assert('$attributeNames[0] == "status"');
    		$modelClassName = $this->getModelClassName();
    		$value          = ImportSanitizerUtil::
    		sanitizeValueBySanitizerTypes(static::getSanitizerUtilTypesInProcessingOrder(),
    				$modelClassName, null, $value, $columnName, $columnMappingData, $importSanitizeResultsUtil);
    		return array('status' => $value);
    	}
    }
    
?>