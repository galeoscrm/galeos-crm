<?php
    /**
     * Defines the dedupe rules when creating a new Contract
     */
    class ContractDedupeRules extends DedupeRules
    {
        /**
         * @see DedupeRules
         */
        protected function getDedupeAttributesAndRelatedAttributesMappedArray()
        {
            return array(
            		'localContractId' => null,
            		'name' => null,
            );
        }

        /**
         * @see DedupeRules
         */
        protected function getDedupeElements()
        {
            return array('Text');
        }

        /**
         * @see DedupeRules
         */
        protected function getDedupeAttributesAndSearchForDuplicateModelsCallbackMappedArray()
        {
            return array(
                'localContractId'          => 'ContractSearch::getContractsByLocalContractId',
            	'name'                     => 'ContractSearch::getContractsByName',
            );
        }
        
        public function getDedupeViewClassName()
        {
        	return 'ContractsToMergeListAndChartView';
        }
        
    }
?>