<?php
   
    /**
     * Class defining rules for Contract gamification behavior.
     */
    class ContractGamificationRules extends GamificationRules
    {
        public static function getPointTypesAndValuesForCreateModel()
        {
            return array(GamePoint::TYPE_SALES => 15);
        }

        public static function getPointTypesAndValuesForUpdateModel()
        {
            return array(GamePoint::TYPE_SALES => 15);
        }
    }
?>