<?php
      /**
     * Workflow rules to be used with the Accounts module.
     */
    class ContractsWorkflowRules extends SecuredWorkflowRules
    {
        /**
         * @return array
         */
        public static function getDefaultMetadata()
        {
            $metadata = array(
                'Contract' => array(
                    'cannotTrigger' =>
                        array('files', 'notificationSubscribers', 'comments')
                    ),
            );
            return array_merge(parent::getDefaultMetadata(), $metadata);
        }
    }
?>