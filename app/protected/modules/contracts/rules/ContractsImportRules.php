<?php
    /**
     * Defines the import rules for importing into the accounts module.
     */
    class ContractsImportRules extends ImportRules
    {
        public static function getModelClassName()
        {
            return 'Contract';
        }

        /**
         * Get fields for which dedupe ruled would be executed
         * @return array
         */
        public static function getDedupeAttributes()
        {
            return array('localContractId');
        }
    }
?>