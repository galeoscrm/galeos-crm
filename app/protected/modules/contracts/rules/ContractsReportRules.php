<?php
    /**
     * Report rules to be used with the Accounts module.
     */
    class ContractsReportRules extends SecuredReportRules
    {
        /**
         * @return array
         */
        public static function getDefaultMetadata()
        {
            $metadata = array(
                'Contract' => array(
                    'nonReportable' =>
                        array('notificationSubscribers', 'comments', 'files'),
                )
            );
            return array_merge(parent::getDefaultMetadata(), $metadata);
        }
    }
?>