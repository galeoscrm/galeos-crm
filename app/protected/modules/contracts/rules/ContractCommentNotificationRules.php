<?php

    /**
     * A  NotificationRules to manage when a new comment is added to a contract or when updated existing
     */
    class ContractCommentNotificationRules extends CommentNotificationRules
    {
        /**
         * @inheritdoc
         */
        public function getModuleClassNames()
        {
            return array('ContractsModule');
        }

        /**
         * @returns Translated label that describes this rule type.
         */
        public function getDisplayName()
        {
            return Zurmo::t('ContractsModule', 'Contract comments creation or modification');
        }

        /**
         * @return The type of the NotificationRules
         */
        public function getType()
        {
            return 'ContractComment';
        }

        public function getTooltipId()
        {
            return 'contract-comment-notification-tooltip';
        }

        public function getTooltipTitle()
        {
            return Zurmo::t('UsersModule', 'Notify me when new comment is added or updated existing on contracts I am following.');
        }
    }
?>