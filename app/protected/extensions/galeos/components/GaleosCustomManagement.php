<?php
	/**
	 * 
	 * @author galeos
	 *  To invoke either run zurmo installation from scratch
	 *  or for updating existing installation do GET request from browser with resolveCustomData=1 parameter
	 *  i.e.http://localhost/zurmo/app/index.php/home/default?resolveCustomData=1    
	 */
    class GaleosCustomManagement extends CustomManagement
    {
        /**
         * (non-PHPdoc)
         * @see CustomManagement::runAfterInstallationDefaultDataLoad()
         */
        public function runAfterInstallationDefaultDataLoad(MessageLogger $messageLogger)
        {
        	LinksInstallUtil::resolveCustomMetadataAndLoad();
        	ContractsInstallUtil::resolveCustomMetadataAndLoad();
        	AccountSyncInstallUtil::resolveCustomMetadataAndLoad();
        	ContactSyncInstallUtil::resolveCustomMetadataAndLoad();
        }

        /**
         * (non-PHPdoc)
         * @see CustomManagement::resolveIsCustomDataLoaded()
         */
        public function resolveIsCustomDataLoaded()
        {
        	LinksInstallUtil::resolveCustomMetadataAndLoad();
        	ContractsInstallUtil::resolveCustomMetadataAndLoad();
        	AccountSyncInstallUtil::resolveCustomMetadataAndLoad();
        	ContactSyncInstallUtil::resolveCustomMetadataAndLoad();
        }
        
	    public function resolveAccountCustomActionsBeforeSave($account)
    	{
	    	$accountSync = AccountSync::getByAccount($account);
	    	if ($accountSync == null)
	    	{
    			$accountSync = new AccountSync();
    			$accountSync->account = $account;
	    	}
	    	$accountSync->processed		= false;
	    	$accountSync->processedTime	= null;
	    	$accountSync->save();
     	}
     	
     	public function resolveContactCustomActionsBeforeSave($contact)
     	{
     		$contactSync = ContactSync::getByContact($contact);
     		if ($contactSync == null)
     		{
     			$contactSync = new ContactSync();
     			$contactSync->contact = $contact;
     			$contactSync->account = $contact->account;
     		}
     		$contactSync->processed		= false;
     		$contactSync->processedTime	= null;
     		$contactSync->save();
     	}
    }
?>