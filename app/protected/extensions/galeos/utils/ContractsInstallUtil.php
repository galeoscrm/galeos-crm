<?php
    class ContractsInstallUtil
    {
        public static function resolveCustomMetadataAndLoad()
        {
            $shouldSaveZurmoModuleMetadata = false;
            $metadata                      = ZurmoModule::getMetadata();
            if(!in_array('contracts', $metadata['global']['tabMenuItemsModuleOrdering']))
            {
            	$metadata['global']['tabMenuItemsModuleOrdering'][] = 'contracts';
            	$shouldSaveZurmoModuleMetadata = true;
            }
            if($shouldSaveZurmoModuleMetadata)
            {
            	ZurmoModule::setMetadata($metadata);
            	GeneralCache::forgetAll();
            }
            
            $shouldSaveActivityModuleMetadata = false;
            $metadata                         = Activity::getMetadata();
            if(!in_array('Contract', $metadata['Activity']['activityItemsModelClassNames']))
            {
            	$metadata['Activity']['activityItemsModelClassNames'][] = 'Contract';
            	$shouldSaveActivityModuleMetadata = true;
            }
            if($shouldSaveActivityModuleMetadata)
            {
            	Activity::setMetadata($metadata);
                GeneralCache::forgetAll();
            }
            
            Yii::import('application.extensions.zurmoinc.framework.data.*');
            Yii::import('application.modules.contracts.data.*');
        }
    }
?>