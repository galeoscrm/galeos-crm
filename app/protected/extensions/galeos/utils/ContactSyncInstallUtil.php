<?php
    class ContactSyncInstallUtil
    {
        public static function resolveCustomMetadataAndLoad()
        {
            $shouldSaveActivityModuleMetadata = false;
            $metadata                         = Activity::getMetadata();
            
            if(!in_array('ContactSync', $metadata['Activity']['activityItemsModelClassNames']))
            {
                $metadata['Activity']['activityItemsModelClassNames'][] = 'ContactSync';
                $shouldSaveActivityModuleMetadata = true;
                
            }
            if($shouldSaveActivityModuleMetadata)
            {
            	Activity::setMetadata($metadata);
                GeneralCache::forgetAll();
            }
            Yii::import('application.extensions.zurmoinc.framework.data.*');
            Yii::import('application.modules.contactsync.data.*');
         }
    }
?>