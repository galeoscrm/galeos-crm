<?php
    class LinksInstallUtil
    {
        public static function resolveCustomMetadataAndLoad()
        {
            $shouldSaveActivityModuleMetadata = false;
            $metadata                         = Activity::getMetadata();
            
            if(!in_array('Link', $metadata['Activity']['activityItemsModelClassNames']))
            {
                $metadata['Activity']['activityItemsModelClassNames'][] = 'Link';
                $shouldSaveActivityModuleMetadata = true;
                
            }
            if($shouldSaveActivityModuleMetadata)
            {
            	Activity::setMetadata($metadata);
                GeneralCache::forgetAll();
            }
            Yii::import('application.extensions.zurmoinc.framework.data.*');
            Yii::import('application.modules.links.data.*');
         }
    }
?>