<?php
    class AccountSyncInstallUtil
    {
        public static function resolveCustomMetadataAndLoad()
        {
            $shouldSaveActivityModuleMetadata = false;
            $metadata                         = Activity::getMetadata();
            
            if(!in_array('AccountSync', $metadata['Activity']['activityItemsModelClassNames']))
            {
                $metadata['Activity']['activityItemsModelClassNames'][] = 'AccountSync';
                $shouldSaveActivityModuleMetadata = true;
                
            }
            if($shouldSaveActivityModuleMetadata)
            {
            	Activity::setMetadata($metadata);
                GeneralCache::forgetAll();
            }
            Yii::import('application.extensions.zurmoinc.framework.data.*');
            Yii::import('application.modules.accountsync.data.*');
         }
    }
?>