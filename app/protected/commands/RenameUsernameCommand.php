<?php
    class RenameUsernameCommand extends CConsoleCommand
    {
    	const DEBUG = false;
    	
        public function getHelp()
        {
            return <<<EOD
    USAGE
      zurmoc RenameUsername <username> <olduser> <newuser> 

    DESCRIPTION
      Rename user

    PARAMETERS
    * username: username to log in as and run the process. Typically 'super'.


EOD;
    }

    public function run($args)
    {
    	
    	
    	if (count($args) != 3)
    	{
    		$this->usageError('Syntax error');
    	}
    	
        try
        {
        	Yii::app()->user->userModel = User::getByUsername($args[0]);
        }
        catch (NotFoundException $e)
        {
        	self::Error("The specified username " . $args[0] . " does not exist.", true);
        }
        
        try
        {
        	$olduser = User::getByUsername($args[1]);
        }
        catch (NotFoundException $e)
        {
        	self::Error("The specified username " . $args[1] . " does not exist.", true);
        }
        
        echo "Changing from $olduser->username to $args[2]\n";
        $olduser->username = $args[2]; 
        $olduser->save();
        
    }
    
    
    public function Error($message, $shouldExit = false)
    {
    	echo "Error: $message\n\n";
    	if ($shouldExit)
    	{
    		exit(1);
    	}
    }
}
?>