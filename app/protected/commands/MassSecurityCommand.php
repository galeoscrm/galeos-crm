<?php
    ini_set('memory_limit', '1556M');
    class MassSecurityCommand extends CConsoleCommand
    {
        public function getHelp()
        {
            return <<<EOD
    USAGE
      zurmoc MassSecurity <username> <group>

    DESCRIPTION
      Removes all permisions on accounts and related contacts and sets RW permission for a configured group

    PARAMETERS
    * username: username to log in as and run the import processes. Typically 'super'.
    * group: group name

EOD;
    }

    public function run($args)
    {
    	echo PHP_EOL,"MassSecurity tool started",PHP_EOL;
    	
    	if (count($args) != 2)
    	{
    		$this->usageError('Syntax error');
    	}
    	
    	if (!isset($args[0]))
    	{
    		$this->usageError('A username must be specified.');
    	}
        
        try
        {
        	Yii::app()->user->userModel = User::getByUsername($args[0]);
            Yii::app()->timeZoneHelper->load();
        }
        catch (NotFoundException $e)
        {
        	$this->usageError('The specified username does not exist.');
        }
        
        try
        {
        	$permitable = Group::getByName($args[1]);
        }
        catch (NotFoundException $e)
        {
        	$this->usageError('The specified group does not exist.');
        }
        
		$accounts = self::getAllAccounts();
		
		foreach ( $accounts as $account) {
				
				echo 'Setting up security for account   ',$account -> name,'(',$account -> id,')',PHP_EOL;
				$account->removeAllPermissions();
				$account->addPermissions ( $permitable, 27);
				$account->save();
				$contacts = self::getAccountContacts($account);
				foreach ( $contacts as $contact) {
					echo 'Setting up security for   contact ',$contact->lastName,'(',$contact->id,')',PHP_EOL;
					$contact->removeAllPermissions();
					$contact->addPermissions ( $permitable, 27);
					$contact->save();
				}
			
		}
		echo 'Rebuilding Read Permissions',  PHP_EOL;
		ReadPermissionsOptimizationUtil::rebuild();
    }
    
    public static function getAccountContacts($account)
    {
    	$metadata = array();
    	$metadata['clauses'] = array(
    			1 => array(
    					'attributeName'        => 'account',
    					'operatorType'         => 'equals',
    					'value'                =>  $account->id,
    			),
    	);
    	$metadata['structure'] = '1';
    	$joinTablesAdapter   = new RedBeanModelJoinTablesQueryAdapter('Contact');
    	$where  = RedBeanModelDataProvider::makeWhere('Contact', $metadata, $joinTablesAdapter);
    	return Contact::getSubset($joinTablesAdapter, null, null, $where );
    }
    
    public static function getAllAccounts($namepart = null)
    {

    	$metadata = array();
    	$metadata['clauses'] = array(
    			1 => array(
    					'attributeName'        => 'name',
    					'operatorType'         => 'contains',
    					'value'                => $namepart,
    			),
    	);
    	
    	$metadata['structure'] = '1';
    	$joinTablesAdapter   = new RedBeanModelJoinTablesQueryAdapter('Account');
    	if ($namepart != null)
    		$where  = RedBeanModelDataProvider::makeWhere('Account', $metadata, $joinTablesAdapter);
    	else 
    		$where = null;
    	return Account::getSubset($joinTablesAdapter, null, null, $where );
    }
}
?>
