<?php
    class LdapSyncCommand extends CConsoleCommand
    {
    	const DEBUG = false;
    	
        public function getHelp()
        {
            return <<<EOD
    USAGE
      zurmoc LdapSync <username> <groupname>

    DESCRIPTION
      Synchronizes Kolab Groupware LDAP user accounts with Zurmo user profiles

    PARAMETERS
    * username: username to log in as and run the process. Typically 'super'.
    * groupname: Name of LDAP group the user must be member of to be synced. 
                 Optional parameter. Default value is 'CRM Users'

EOD;
    }

    public function run($args)
    {
    	self::Info("LdapSync tool started");
    	
    	if (count($args) > 2 || count($args) == 0)
    	{
    		$this->usageError('Syntax error');
    	}
    	if (!isset($args[0]))
    	{
    		$this->usageError('A username must be specified.');
    	}
        try
        {
        	Yii::app()->user->userModel = User::getByUsername($args[0]);
        }
        catch (NotFoundException $e)
        {
        	self::Error('The specified username does not exist.', true);
        }
        if (count($args) == 2)
        {
        	$groupName = $args[1];
        }
        else {
        	$groupName = 'CRM Users';
        }
        
        Yii::app()->timeZoneHelper->load();
        $ldapEnabled 			   = Yii::app()->authenticationHelper->ldapEnabled;
        if (!$ldapEnabled)
        {
        	self::Error('LDAP Authentication is not enabled in configuration', true);
        }
        // Connect to LDAP
        $serverType                = Yii::app()->authenticationHelper->ldapServerType;
        $host                      = Yii::app()->authenticationHelper->ldapHost;
        $port                      = Yii::app()->authenticationHelper->ldapPort;
        $baseDomain                = Yii::app()->authenticationHelper->ldapBaseDomain;
        $bindPassword              = Yii::app()->authenticationHelper->ldapBindPassword;
        $bindRegisteredDomain      = Yii::app()->authenticationHelper->ldapBindRegisteredDomain;
        $ldapConnection            = LdapUtil::establishConnection($serverType, $host, $port, $bindRegisteredDomain,
        		$bindPassword, $baseDomain);
        if ($ldapConnection)
        {
        	self::Debug("Connected to LDAP(" . $host . ":" . $port . ";" .$baseDomain . ")");
        	self::synchronize($ldapConnection,$baseDomain, $groupName);
        }	
		else
		{
			self::Error('Failed to connect to LDAP', true);
		}
		
		self::Info("LdapSync tool finished");
    }
    
    protected function synchronize($ldapConnection,$baseDomain, $groupName)
    {
    	$groupCN = "cn=" . $groupName;
    	// Get CRM users group members
    	try {
    		$baseDomain4Group = str_replace("ou=People", "ou=Groups", $baseDomain);
    		$groupMembers = self::getLDAPGroupMembers($ldapConnection, $baseDomain4Group, $groupCN);
    		self::Debug("Group '" . $groupName . "' has " . $groupMembers["count"] . " members");
    	}
    	catch (NotFoundException $e)
    	{
    		self::Error('Group not found', true);
    	}
    	// Get LDAP users
    	$ldapFilter = "(objectClass=person)";
    	$justthese = array("cn","sn","givenname","mail","uid");
    	$ldapResults             = ldap_search($ldapConnection, $baseDomain, $ldapFilter, $justthese);
    	$ldapResultsCount        = ldap_count_entries($ldapConnection, $ldapResults);
    	self::Debug("Got total $ldapResultsCount ldap users");
    	if ($ldapResultsCount > 0)
    	{
    		$results = @ldap_get_entries($ldapConnection, $ldapResults);
    		$syncedUsers = array();
    		$counter = 0;
    		// For each LDAP user
    		for ($i=0; $i<$results["count"]; $i++) {
    			$userDN = $results[$i]["dn"];
    			// Is member of CRM Users group ?
    			if (in_array($userDN,$groupMembers))
    			{
    				$userData = self::extractUserDataFromLdapResult($results[$i]);
    				$username = $userData['username'];
    				self::Info("Syncing user " . $username . "(dn:" . $userDN . ") with LDAP");
    				// Get Zurmo user and sync it
    				try
    				{
    					$zurmoUser = Yii::app()->user->userModel = User::getByUsername($username);
    					$flagSavedNecessary = self::mapExistingUser($zurmoUser, $userData);
    					if ($flagSavedNecessary) 
    					{
    						$zurmoUser->save();
    					}
    					else 
    					{
    						self::Debug("*no changes");
    					}
	   				}
    				catch (NotFoundException $e)
    				{
    					$zurmoUser = new User();
    					$flagSavedNecessary = self::mapNewUser($zurmoUser,$userData);
    					$zurmoUser->save();
    					self::Info("*new user profile created in Zurmo");
    				}
    				$syncedUsers[$counter] = $username;
    				$counter++;
    			} // end of if
    		} // end of for
    		// Disable all other users active users
    		self::Debug("Disabling non member(" . $groupName . ") users in Zurmo");
    		$activeZurmoUsers = User::getActiveUsers(false);
    		$flagChanged = false;
    		for ($i=0; $i<count($activeZurmoUsers); $i++) {
    			$zurmoUser = $activeZurmoUsers[$i];
    			if (!in_array($zurmoUser->username,$syncedUsers)) {
    				$flagChanged = true;
    				self::disableZurmoUser($zurmoUser);
    				$zurmoUser->save();
    				self::Info("*user " . $zurmoUser->username . " disabled");
    			}
    		}
    		if (!$flagChanged) {
    			self::Debug("*no changes");
    		}
    	}
    }
    
    protected function getLDAPGroupMembers($ldapConnection, $baseDomain, $groupCN)
    {
    	$ldapFilter        = "(&(objectClass=groupofuniquenames)(" . $groupCN . "))";
    	$ldapResults       = ldap_search($ldapConnection, $baseDomain, $ldapFilter);
    	$ldapResultsCount  = ldap_count_entries($ldapConnection, $ldapResults);
    	if ($ldapResultsCount = 1)
    	{
    		$results = @ldap_get_entries($ldapConnection, $ldapResults);
    		if ($results["count"] > 0 ){
    			return $results[0]["uniquemember"];
    		}
    		else {
    			throw new NotFoundException("Group with cn:" . $groupCN . " not foundin LDAP");
    		}
    	}
    }
    
    protected function enableZurmoUser($user)
    {
    	$user->setRight('UsersModule', UsersModule::RIGHT_LOGIN_VIA_WEB,     Right::ALLOW);
	    $user->setRight('UsersModule', UsersModule::RIGHT_LOGIN_VIA_MOBILE,  Right::ALLOW);
    	$user->setRight('UsersModule', UsersModule::RIGHT_LOGIN_VIA_WEB_API, Right::ALLOW);
    }
    
    protected function disableZurmoUser($user)
    {
    	$user->setRight('UsersModule', UsersModule::RIGHT_LOGIN_VIA_WEB,     Right::DENY);
    	$user->setRight('UsersModule', UsersModule::RIGHT_LOGIN_VIA_MOBILE,  Right::DENY);
    	$user->setRight('UsersModule', UsersModule::RIGHT_LOGIN_VIA_WEB_API, Right::DENY);
    }
    
    protected function extractUserDataFromLdapResult($ldapResult) 
    {
    	$userData =  array ();
		$userData ['username'] = $ldapResult ["uid"] [0];
		if (isset ( $ldapResult ["mail"] )) {
			$userData ['mail'] = $ldapResult ["mail"] [0];
		} else {
			$userData ['mail'] = '';
		}
		if (isset ( $ldapResult ["givenname"] )) {
			$userData ['firstName']  = $ldapResult ["givenname"] [0];
		} else {
			$userData ['firstName']  = "";
		}
		if (isset ( $ldapResult ["sn"] )) {
			$userData ['lastName']  = $ldapResult ["sn"] [0];
		} else {
			$userData ['lastName'] = $userUsername;
		}
		return $userData; 
	}
	
    protected function mapNewUser($user, $userData)
    {
    	$user->lastName = $userData['lastName'];
    	$user->primaryEmail->emailAddress = $userData['mail'];;
    	$user->firstName = $userData['firstName'];
    	$user->username = $userData['username'];
    	$user->setPassword('LDAP');
    }
    
    protected function mapExistingUser($user, $userData)
    {
    	$flagSavedNecessary = false;
    	if ($user->lastName != $userData['lastName'])
    	{
    		$flagSavedNecessary = true;
    		$user->lastName = $userData['lastName'];
    		self::Debug("*lastName updated");
    	}
    	if ($user->firstName != $userData['firstName'])
    	{
    		$flagSavedNecessary = true;
    		$user->firstName = $userData['firstName'];
    		self::Debug("*firstName updated");
    	}
    	if ($user->primaryEmail->emailAddress != $userData['mail'])
    	{
    		$flagSavedNecessary = true;
    		$user->primaryEmail->emailAddress = $userData['mail'];
    		self::Debug("*emailAddress updated");
    	}
    	// if user not active make it active
    	if (!$user->isActive) {
    		$flagSavedNecessary = true;
    		self::enableZurmoUser($user);
    		self::Debug("*isActive updated");
    	}
    	return $flagSavedNecessary;
    }
    
    public function Debug($message)
    {
    	if (self::DEBUG) {
    		echo "DEBUG: $message\n";
    	}
    }
    
    public function Info($message)
    {
    	echo "INFO: $message\n";
    }
    
    public function Error($message, $shouldExit = false)
    {
    	echo "Error: $message\n\n";
    	if ($shouldExit)
    	{
    		exit(1);
    	}
    }
}
?>