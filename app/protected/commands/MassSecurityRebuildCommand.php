<?php
    ini_set('memory_limit', '1556M');
    class MassSecurityRebuildCommand extends CConsoleCommand
    {
        public function getHelp()
        {
            return <<<EOD
    USAGE
      zurmoc MassSecurityRebuild <username>

    DESCRIPTION

    PARAMETERS
    * username: username to log in as and run the import processes. Typically 'super'.

EOD;
    }

    public function run($args)
    {
    	echo PHP_EOL,"MassSecurityRebuild tool started",PHP_EOL;
    	
    	if (count($args) != 1)
    	{
    		$this->usageError('Syntax error');
    	}
    	
    	if (!isset($args[0]))
    	{
    		$this->usageError('A username must be specified.');
    	}
        
        try
        {
        	Yii::app()->user->userModel = User::getByUsername($args[0]);
            Yii::app()->timeZoneHelper->load();
        }
        catch (NotFoundException $e)
        {
        	$this->usageError('The specified username does not exist.');
        }
        
	echo 'Rebuilding Read Permissions',  PHP_EOL;
	ReadPermissionsOptimizationUtil::rebuild();
    }
}
