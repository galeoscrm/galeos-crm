<?php
	$customModules = array(
			'links',
            'contracts',
			'accountSync',
			'contactSync',
    );

    $instanceConfig   = array(
        'modules' => $customModules,
    );
    
    $instanceConfig['components']['custom']['class'] =  'application.extensions.galeos.components.GaleosCustomManagement';
    
    $instanceConfig['import'][] = "application.extensions.galeos.*";                          
    $instanceConfig['import'][] = "application.extensions.galeos.components.*";               
    $instanceConfig['import'][] = "application.extensions.galeos.utils.*";                    

    foreach ($customModules as $index => $moduleName)
    {
        $instanceConfig['import'][] = "application.modules.$moduleName.*";                           
        $instanceConfig['import'][] = "application.modules.$moduleName.adapters.*";                  
        $instanceConfig['import'][] = "application.modules.$moduleName.adapters.columns.*";          
        $instanceConfig['import'][] = "application.modules.$moduleName.dataproviders.*";             
        $instanceConfig['import'][] = "application.modules.$moduleName.elements.*";                  
        $instanceConfig['import'][] = "application.modules.$moduleName.elements.actions.*";          
        $instanceConfig['import'][] = "application.modules.$moduleName.elements.actions.security.*"; 
        $instanceConfig['import'][] = "application.modules.$moduleName.elements.derived.*";          
        $instanceConfig['import'][] = "application.modules.$moduleName.components.*";                
        $instanceConfig['import'][] = "application.modules.$moduleName.controllers.*";               
        $instanceConfig['import'][] = "application.modules.$moduleName.controllers.filters.*";       
        $instanceConfig['import'][] = "application.modules.$moduleName.exceptions.*";                
        $instanceConfig['import'][] = "application.modules.$moduleName.forms.*";                     
        $instanceConfig['import'][] = "application.modules.$moduleName.forms.attributes.*";          
        $instanceConfig['import'][] = "application.modules.$moduleName.interfaces.*";                
        $instanceConfig['import'][] = "application.modules.$moduleName.models.*";                    
        $instanceConfig['import'][] = "application.modules.$moduleName.modules.*";                   
        $instanceConfig['import'][] = "application.modules.$moduleName.rules.*";                     
        $instanceConfig['import'][] = "application.modules.$moduleName.rules.attributes.*";          
        $instanceConfig['import'][] = "application.modules.$moduleName.rules.policies.*";            
        $instanceConfig['import'][] = "application.modules.$moduleName.tests.unit.*";                
        $instanceConfig['import'][] = "application.modules.$moduleName.tests.unit.files.*";          
        $instanceConfig['import'][] = "application.modules.$moduleName.tests.unit.models.*";         
        $instanceConfig['import'][] = "application.modules.$moduleName.tests.unit.walkthrough.*";    
        $instanceConfig['import'][] = "application.modules.$moduleName.utils.*";                     
        $instanceConfig['import'][] = "application.modules.$moduleName.utils.charts.*";              
        $instanceConfig['import'][] = "application.modules.$moduleName.utils.sanitizers.*";          
        $instanceConfig['import'][] = "application.modules.$moduleName.utils.security.*";            
        $instanceConfig['import'][] = "application.modules.$moduleName.utils.analyzers.*";           
        $instanceConfig['import'][] = "application.modules.$moduleName.validators.*";                
        $instanceConfig['import'][] = "application.modules.$moduleName.views.*";                     
        $instanceConfig['import'][] = "application.modules.$moduleName.views.attributetypes.*";      
        $instanceConfig['import'][] = "application.modules.$moduleName.views.charts.*";              
        $instanceConfig['import'][] = "application.modules.$moduleName.views.related.*";             
        $instanceConfig['import'][] = "application.modules.$moduleName.widgets.*";                   
    }
    
?>
